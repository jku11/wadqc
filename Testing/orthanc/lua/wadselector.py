#!/usr/bin/env python
import subprocess

__version__ = '20160419'

"""
wadselector should be the process that runs the real selector in the background,
because the lua script in orthanc halts orthanc during execution of the lua script 
(https://orthanc.chu.ulg.ac.be/book/users/lua.html : All of these callbacks are 
guaranteed to be invoked in mutual exclusion), meaning that the selector cannot
access orthanc.
"""

if __name__ == "__main__":
    import os
    import sys
    
    root = os.path.dirname(os.getcwd())
    cmd = [os.path.join(root, 'wad_core', 'selector.py')]
    cmd.extend(sys.argv[1:])
    cmd.extend(["-i", "test_wadconfig.ini"])
    print(cmd)
    subprocess.Popen(cmd)
