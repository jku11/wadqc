function OnStableStudy(studyId, tags, metadata)
   if (metadata['ModifiedFrom'] == nil and
       metadata['AnonymizedFrom'] == nil) then

      print('This study is now stable: ' .. studyId)
      
      -- Call WAD_Collector
      os.execute('orthanc/lua/wadselector.py --studyid ' .. studyId)
   end
end
