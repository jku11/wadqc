#!/usr/bin/env python
import requests
import json, jsmin
import time
import argparse
import sys

BASEURL = "http://localhost:3000/api"

class MyException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

def broken_url(method):
    url = "{}/broken_broken_broken".format(BASEURL)
    payload = {'foo': 1, 'bar': 'bar'}
    response = getattr(requests, method)(url, json=payload)
    try:
        return json.loads(response.text)
    except Exception as e:
        return {'success': False, 'msg':str(e)}
    
def test_broken_url():
    print("== Testing non-existing url...")
    t0 = time.time()
    methods = ['post', 'get', 'delete', 'put']
    badmethods = []
    try:
        for method in methods:
            result = broken_url(method)
            print("... {}:{}".format(method, result['msg']))
            if result["success"]:
                raise MyException("Reached non-existing url with {}".format(method))
            badmethods.append(method)
    except MyException as e:
        print("ERROR! {}".format(str(e)))
        
    elapsed = time.time()-t0
    print("== Successfully tested methods [{}] for non-existing url in {} s".format(', '.join(badmethods), elapsed))
    

#@flask_app.route('/api/authenticate', methods=['POST'])
def get_access_token(user, pswd):
    """
    curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' http://localhost:12002/api/authenticate
    """
    url = "{}/authenticate".format(BASEURL)
    payload = {'username': user, 'password': pswd}
    response = requests.post(url, json=payload)
    return json.loads(response.text)

def auth_header(token):
    return {'Authorization': 'JWT {}'.format(token)}

## protected routes
#@flask_app.route('/api/verifytoken', methods=['POST'])
def verifytoken(token):
    """
    """
    url = "{}/verifytoken".format(BASEURL)
    payload = auth_header(token)
    response = requests.post(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors', methods=['GET'])
def selectors(token):
    """
    "Authorization: Bearer $ACCESS"
    """
    url = "{}/selectors".format(BASEURL)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>', methods=['GET'])
def selectors_id(token, id_selector):
    """
    "Authorization: Bearer $ACCESS"
    """
    url = "{}/selectors/{}".format(BASEURL, id_selector)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results', methods=['GET'])
def selectors_id_results(token, id_selector):
    """
    "Authorization: Bearer $ACCESS"
    """
    url = "{}/selectors/{}/results".format(BASEURL, id_selector)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>', methods=['GET'])
def selectors_id_results_id(token, id_selector, id_result):
    """
    "Authorization: JWT $ACCESS"
    """
    url = "{}/selectors/{}/results/{}".format(BASEURL, id_selector, id_result)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/dicom',methods=['GET'])
def selectors_id_results_id_dicom(token, id_selector, id_result):
    """
    "Authorization: JWT $ACCESS"
    """
    url = "{}/selectors/{}/results/{}/dicom".format(BASEURL, id_selector, id_result)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results/last', methods=['GET'])
def selectors_id_results_last(token, id_selector):
    """
    "Authorization: JWT $ACCESS"
    """
    url = "{}/selectors/{}/results/last".format(BASEURL, id_selector)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:type>', methods=['GET'])
def selectors_id_results_id_tests_id_path(token, id_selector, id_result, id_test, rtype):
    """
    "Authorization: JWT $ACCESS"
    """
    url = "{}/selectors/{}/results/{}/tests/{}/{}".format(BASEURL, id_selector, id_result, id_test, rtype)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

## unprotected routes
#@flask_app.route('/api/wadselector', methods=['GET'])
def wadselector(source, studyid):
    """
    """
    url = "{}/wadselector".format(BASEURL)
    payload = {'studyid':studyid, 'source':source}

    response = requests.get(url, params=payload)
    return json.loads(response.text)

## test access routes
#@flask_app.route('/api/testaccess_minor', methods=['GET'])
def testaccess_minor(token):
    """
    test if rest_minor access granted
    """
    url = "{}/testaccess_minor".format(BASEURL)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/testaccess_major', methods=['GET'])
def testaccess_major(token):
    """
    test if rest_major access granted
    """
    url = "{}/testaccess_major".format(BASEURL)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/testaccess_full', methods=['GET'])
def testaccess_full(token):
    """
    test if rest_full access granted
    """
    url = "{}/testaccess_full".format(BASEURL)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)



# tests of endpoints
def test_access(token):
    """
    Test access to protected routes.
    """
    
    print("== Testing access...")
    testcount = 0

    t0 = time.time()
    try:
        result = testaccess_minor(token)
        if result.get("success", None):
            print("...Granted rest_minor access")
        else:
            print("...NOT Granted rest_minor access")
        testcount += 1

        result = testaccess_major(token)
        if result.get("success", None):
            print("...Granted rest_major access")
        else:
            print("...NOT Granted rest_major access")
        testcount += 1

        result = testaccess_full(token)
        if result.get("success", None):
            print("...Granted rest_full access")
        else:
            print("...NOT Granted rest_full access")
        testcount += 1
    
    except MyException as e:
        print("ERROR! {}".format(str(e)))

    elapsed = time.time()-t0
    print("== Successfully tested access {} times in {} s".format(testcount, elapsed))

def test_full_protected(token):
    """
    test all full protected endpoints
    """
    print("== Testing full protected end-points...")
    testcount = 0

    t0 = time.time()
    try:
        # no rest_full endpoints...
        pass
    
    except MyException as e:
        print("ERROR! {}".format(str(e)))

    elapsed = time.time()-t0

    print("== Successfully tested {} full protected end-points in {} s".format(testcount, elapsed))

def test_major_protected(token):
    """
    test all major protected endpoints
    """
    print("== Testing major protected end-points...")
    testcount = 0

    t0 = time.time()
    try:
        # selectors
        result = selectors(token)
        print("... selectors", result)
        if not result.get("success", None):
            raise MyException("Could not access selectors!")
        testcount += 1
    
        daselectors = result['selectors']
        for sel in daselectors:
            result = selectors_id(token, sel['id'])
            print("..... selectors/{}".format(sel['id']), result)
            if not result.get("success", None):
                raise MyException("Could not access selectors/id!")
            testcount += 1
            
            # results
            result = selectors_id_results(token, sel['id'])
            print("....... selectors/{}/results".format(sel['id']), result)
            if not result.get("success", None):
                raise MyException("Could not access selectors/id/results!")
            testcount += 1
    
            data = result['results']
            for dat in data:
                result =  selectors_id_results_id(token, sel['id'], dat['id'])
                print("......... selectors/{}/results/{}".format(sel['id'], dat['id']), result)
                if not result.get("success", None):
                    raise MyException("Could not access selectors/id/results/id!")
                testcount += 1
                tests = result['tests']
            
                result = selectors_id_results_id_dicom(token, sel['id'], dat['id'])
                print("......... selectors/{}/results/{}/dicom".format(sel['id'], dat['id']), result)
                if not result.get("success", None):
                    raise MyException("Could not access selectors/id/results/id/dicom!")
                testcount += 1
                
                for test in tests:
                    result = selectors_id_results_id_tests_id_path(token, sel['id'], dat['id'], test['id'], test['type'])
                    print("......... selectors/{}/results/{}/tests/{}/{}".format(sel['id'], dat['id'], test['id'], test['type']), result)
                    if not result.get("success", None):
                        raise MyException("Could not access selectors/id/results/id/tests/id/type!")
                    testcount += 1
    
            result = selectors_id_results_last(token, sel['id'])
            print("..... selectors/{}/results/last".format(sel['id']), result)
            if not result.get("success", None):
                raise MyException("Could not access selectors/id/results/last!")
            testcount += 1
    except MyException as e:
        print("ERROR! {}".format(str(e)))

    elapsed = time.time()-t0

    print("== Successfully tested {} major protected end-points in {} s".format(testcount, elapsed))


def test_minor_protected(token):
    """
    test all minor protected endpoints
    """
    print("== Testing minor protected end-points...")
    testcount = 0

    t0 = time.time()
    try:
        # token
        result = verifytoken(token)
        print("... verifytoken", result)
        if not result.get("success", None):
            raise MyException("Could not verify token!")
        testcount += 1
    
    except MyException as e:
        print("ERROR! {}".format(str(e)))

    elapsed = time.time()-t0

    print("== Successfully tested {} minor protected end-points in {} s".format(testcount, elapsed))


# get token for access, needed for tests
def get_access(username, password):
    """
    try to get access with provided username and password
    """
    token = None

    try:
        auth = get_access_token(username, password)

        if not auth["success"]:
            raise MyException("Could not create token!")
    
        token = auth["token"]
        
    
    except MyException as e:
        print("ERROR! {}".format(str(e)))

    return token

def test_unprotected():
    """
    test all unprotected endpoints
    """
    print("== Testing unprotected end-points...")
    testcount = 0

    t0 = time.time()
    # token
    try:
        result = wadselector(source="WADQC", studyid="bert")
        print("... wadselector", result)
    
        if not result.get("success", None):
            raise MyException("Could not access wadselector!")
        testcount += 1

    except MyException as e:
        print("ERROR! {}".format(str(e)))
        
    elapsed = time.time()-t0
    print("== Successfully tested {} unprotected end-points in {} s".format(testcount, elapsed))

def test_real_user(token):
    """
    test access for real user (should be created through wad_admin)
    """
    print("==== Testing real user")
    t0 = time.time()

    test_broken_url()

    test_unprotected()

    test_access(token)

    test_minor_protected(token)

    test_major_protected(token)

    test_full_protected(token)

    elapsed = time.time()-t0
    print("==== Successfully tested real user in {} s".format(elapsed))
    
    
if __name__ == "__main__":
    """
    test all defined routes of wad api
    """
    epilog = ""
    parser = argparse.ArgumentParser(description='wad-api tests', epilog=epilog)
    parser.add_argument('-u','--user',
                        type=str,
                        default=None,
                        help='username for user with rest_* access rights [None]', dest='username')

    parser.add_argument('-p','--pswd',
                        type=str,
                        default=None,
                        help='password for user with rest_* access rights [None]', dest='password')

    # test real user (make yourself)
    args = parser.parse_args()
    if args.username is None or args.password is None:
        parser.print_help()
        sys.exit()
        
    token = get_access(args.username, args.password)
    if token is None:
        print("Could not get rest access with provided credentials. Exit.")
        sys.exit()

    test_real_user(token)
    
    print("Done.")
    

    

    
    
