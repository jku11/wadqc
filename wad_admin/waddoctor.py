#!/usr/bin/env python
from __future__ import print_function
import os
import sys
import time
import argparse
import codecs

try:
    from . import wadservices_communicate as wad_com
except Exception as e:
    import wadservices_communicate as wad_com

__version__ = 20180910
"""
Changelog:
  20180910: convert all configs in database to new format (add templateversion parameter, 
            comments/info is copied structure of actions)
  20180629: show (more) help
  20180404: added dbupgrade, check
  20180314: based on wadservices 20180214; added drop command for wadadmin and waddashboard
"""
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

def split_versionlabel(version):
    """
    input: version="20180601.8"
    return: 20180601, 8
    """
    
    versiontuple = version.split('.')
    major = int(versiontuple[0])
    try:
        minor = int(versiontuple[1])
    except:
        minor = 0

    return major, minor

def version_smaller(v1, v2):
    """
    compare a two (major, minor) tuples
    """
    if v1[0]<v2[0]:
        return True
    if v1[0] == v2[0] and v1[1]<v2[1]:
        return True
    return False

def version_equal(v1, v2):
    """
    compare a two (major, minor) tuples
    """
    if v1[0] == v2[0] and v1[1] == v2[1]:
        return True
    return False

    
def upgrade_metas(dbio):
    """
    20180910: add metaformat key; 
    """
    error = False
    msg = ""

    latest_format = "20180910"
    import json, jsmin

    count = 0
    for mta in dbio.DBMetaConfigs.select():
        try:
            meta = json.loads(jsmin.jsmin(bytes_as_string(mta.val)))
        except ValueError as e:
            return error, msg

        # check if formatversion defined; if it is, check if it is version 2.0. if so, continue to next meta.
        metaformat = meta.get("metaformat", None)
        if metaformat is None or version_smaller(split_versionlabel(metaformat), split_versionlabel(latest_format)):
            meta["metaformat"] = latest_format          
            mta.val = json.dumps(meta)
            mta.save()
            count += 1

    error = False
    msg = "upgraded {} meta blobs".format(count)
    return error, msg

def upgrade_configs_us(dbio, cfg):
    """
    20180910: add cfgformat key; 
              copy structure of actions[actionname]["params"] to comments[actionname]["params"];
              move comments["params"] content to correct info[actionname]["params"];
              delete comments["params"]
    """
    error = False
    msg = ""

    latest_format = "20180910"
    import json, jsmin
    
    try:
        import app.libs.selectormaintenance as selmain
    except Exception as e:
        import wad_admin.app.libs.selectormaintenance as selmain

    count = 0

    try:
        config = json.loads(jsmin.jsmin(bytes_as_string(cfg.val)))
    except ValueError as e:
        return error, msg

    # check if formatversion defined; if it is, check if it is version 2.0. if so, continue to next config.
    cfgformat = config.get("cfgformat", None)
    if cfgformat is None or version_smaller(split_versionlabel(cfgformat), split_versionlabel(latest_format)):
        """
        3. for each param in comments['params'], check if exists in each action['params']; 
           if so add to info[action][params]
        4. delete comments['params']
        5. set cfgformat to latest
        6. save config
        """
        # ensure the info block exists
        if config.get("info", None) is None:
            config["info"] = {}

        actions = selmain.paramsblock(dbio, cfg.id, logger=None)
        for act in actions:
            act_name = act["action"]
            # make the new action section in the info block
            if not act_name in config['info'].keys():
                config['info'][act_name] = {"params": {}}

            if act_name == "ocr_series":
                # in depth conversion
                # copy actions/params structure from actions to info
                regions = {}
                for par in act["params"]:
                    if par["pvalue"] is None:
                        continue
                    if not par["pname"].startswith("OCR_"):
                        # normal old style param
                        ptrail = json.loads(selmain.base64ToString(par["ptrail"]))
        
                        # find the parent of the param
                        parent = config['actions'][act_name]['params']
                        info = config['info'][act_name]['params']
                        for key in ptrail[:-1]:
                            parent = parent[key]
                            if not key in info:
                                if isinstance(parent, dict):
                                    info[key] = {}
                                elif isinstance(parent, list):
                                    info[key] = []
                            info = info[key]
                            
                        key = ptrail[-1]
                        if 'params' in config['info'].keys() and key in config['info']['params'].keys():
                            info[key] = config['info']['params'][key]
                    else:
                        # build a new dict
                        split = par["pname"].find(':')
                        name = par["pname"][:split]
                        stuff = par["pname"][split+1:]
                        if not name in regions:
                            regions[name] = {}
                        if stuff in ['xywh', 'type', 'prefix', 'suffix']:
                            regions[name][stuff] = par["pvalue"]

                # now new magic
                for key,value in list(config["actions"]["ocr_series"]["params"].items()):
                    if key.startswith("OCR_"):
                        del config["actions"]["ocr_series"]["params"][key]

                config["actions"]["ocr_series"]["params"]["ocr_regions"] = {}
                config["info"]["ocr_series"]["params"]["ocr_regions"] = {}
                for name, region in regions.items():
                    config["actions"]["ocr_series"]["params"]["ocr_regions"][name] = {}
                    config["info"]["ocr_series"]["params"]["ocr_regions"][name] = {}
                    for key, value in region.items():
                        config["actions"]["ocr_series"]["params"]["ocr_regions"][name][key] = value
                        try:
                            oldname = "{}:{}".format(name, key)
                            info = config["comments"]["params"][oldname]
                            config["info"]["ocr_series"]["params"]["ocr_regions"][name][key] = info
                        except:
                            pass
                        
            else: # simple format
                # copy actions/params structure from actions to info
                for par in act["params"]:
                    if par["pvalue"] is None:
                        continue
                    ptrail = json.loads(selmain.base64ToString(par["ptrail"]))
    
                    # find the parent of the param
                    parent = config['actions'][act_name]['params']
                    info = config['info'][act_name]['params']
                    for key in ptrail[:-1]:
                        parent = parent[key]
                        if not key in info:
                            if isinstance(parent, dict):
                                info[key] = {}
                            elif isinstance(parent, list):
                                info[key] = []
                        info = info[key]
                        
                    key = ptrail[-1]
                    if 'params' in config['info'].keys() and key in config['info']['params'].keys():
                        info[key] = config['info']['params'][key]
                
        # delete old structure in comments
        if 'params' in config['comments'].keys():
            del config["comments"]["params"]
        config["cfgformat"] = latest_format          
        cfg.val = json.dumps(config)
        cfg.save()
        count += 1

    error = False
    msg = "upgraded {} us config blobs".format(count)
    return error, count, msg

def upgrade_configs(dbio):
    """
    20180910: add cfgformat key; 
              copy structure of actions[actionname]["params"] to comments[actionname]["params"];
              move comments["params"] content to correct info[actionname]["params"];
              delete comments["params"]
    """
    error = False
    msg = ""

    latest_format = "20180910"
    import json, jsmin
    
    try:
        import app.libs.selectormaintenance as selmain
    except Exception as e:
        import wad_admin.app.libs.selectormaintenance as selmain

    count = 0
    countUS = 0
    for cfg in dbio.DBModuleConfigs.select():
        if cfg.module.name == "US_AirReverberations":
            error, countUS2, msg = upgrade_configs_us(dbio, cfg)
            if not error:
                if countUS2> 0:
                    countUS += countUS2
            if error:
                print(msg)
            continue
        try:
            config = json.loads(jsmin.jsmin(bytes_as_string(cfg.val)))
        except ValueError as e:
            return error, msg

        # check if formatversion defined; if it is, check if it is version 2.0. if so, continue to next config.
        cfgformat = config.get("cfgformat", None)
        if cfgformat is None or version_smaller(split_versionlabel(cfgformat), split_versionlabel(latest_format)):
            """
            3. for each param in comments['params'], check if exists in each action['params']; 
               if so add to info[action][params]
            4. delete comments['params']
            5. set cfgformat to latest
            6. save config
            """
            # ensure the info block exists
            if config.get("info", None) is None:
                config["info"] = {}

            actions = selmain.paramsblock(dbio, cfg.id, logger=None)
            for act in actions:
                act_name = act["action"]

                # make the new action section in the info block
                if not act_name in config['info'].keys():
                    config['info'][act_name] = {"params": {}}

                # copy actions/params structure from actions to info
                for par in act["params"]:
                    if par["pvalue"] is None:
                        continue
                    ptrail = json.loads(selmain.base64ToString(par["ptrail"]))

                    # find the parent of the param
                    parent = config['actions'][act_name]['params']
                    info = config['info'][act_name]['params']
                    for key in ptrail[:-1]:
                        parent = parent[key]
                        if not key in info:
                            if isinstance(parent, dict):
                                info[key] = {}
                            elif isinstance(parent, list):
                                info[key] = []
                        info = info[key]
                        
                    key = ptrail[-1]
                    if 'params' in config['info'].keys() and key in config['info']['params'].keys():
                        info[key] = config['info']['params'][key]
                    
            # delete old structure in comments
            if 'params' in config['comments'].keys():
                del config["comments"]["params"]
            config["cfgformat"] = latest_format          
            cfg.val = json.dumps(config)
            cfg.save()
            count += 1

    error = False
    msg = "upgraded {} config blobs".format(count)
    if countUS>0:
        msg = "\nupgraded {} US config blobs".format(countUS)
    return error, msg

def ensure_postgresql(pgdata):
    """
    make sure a postgresql service is live
    """
    error = False
    msg = ""

    # start postgres
    status = wad_com.postgresql('status', pgdata)
    if not status == 'running':
        status = wad_com.postgresql('start', pgdata)
    if not status == 'running':
        error = True
        msg = "ERROR, cannot start 'postgresql' ({})".format(status)

    return error, msg

def applycmd(services, cmd, inifile, pgdata):
    """
    apply cmd to all services
    """
    if cmd in ['check', 'dbupgrade', 'jsonupgrade']:
        error, msg = ensure_postgresql(pgdata)
        if error:
            print("ERROR! Command {} needs a running PostgreSQL service for WAD. {}".format(cmd, msg))
            return

        try:
            import app.libs.dbupgrade as dbup
        except Exception as e:
            import wad_admin.app.libs.dbupgrade as dbup
        
        # need a dbio object
        if not wad_com.isconnected:
            try:
                wad_com.dbio_connect(inifile)
            except:
                return 'ERROR! cannot connect to WAD-QC database with {}'.format(inifile)
        
        if cmd == 'check':
            for s in services:
                if s == 'dbwadqc':
                    latest_version  = dbup.get_latest_version(wad_com.dbio, logger=None)
                    running_version = dbup.get_running_version(wad_com.dbio, logger=None)
                    if dbup.version_smaller(running_version, latest_version):
                        print("{} {}: An upgrade is available!".format(cmd, s))
                    else:
                        print("{} {}: No upgrade available.".format(cmd, s))
                    print("  Running version {}, latest version {}.".format(running_version, latest_version))
        elif cmd == 'dbupgrade':
            for s in services:
                if s == 'dbwadqc':
                    current_version = dbup.get_running_version(wad_com.dbio, logger=None)
                    error, msgs = dbup.upgrade(wad_com.dbio, logger=None)
                    if error:
                        print("Error running {} for {}! {}".format(cmd, s, msgs))
                        return
                    latest_version  = dbup.get_latest_version(wad_com.dbio, logger=None)
                    running_version = dbup.get_running_version(wad_com.dbio, logger=None)
                    print("Upgraded {} from version {} to {}. Latest version is {}.".format(s, current_version, running_version, latest_version))
                    print("  Restart all wadservices for the changes to take effect.")
        elif cmd == 'jsonupgrade':
            for s in services:
                if s == 'config':
                    error, msgs = upgrade_configs(wad_com.dbio)
                    if error:
                        print("Error running {} for {}! {}".format(cmd, s, msgs))
                        return
                    print(msgs)
                    #print("  Restart all wadservices for the changes to take effect.")
                if s == 'meta':
                    error, msgs = upgrade_metas(wad_com.dbio)
                    if error:
                        print("Error running {} for {}! {}".format(cmd, s, msgs))
                        return
                    print(msgs)
                    #print("  Restart all wadservices for the changes to take effect.")
        return
    elif cmd in ['drop']:
        for s in services:
            # postgres is a special case
            print('* %s %s:...'%(cmd, s), end='')
            sys.stdout.flush()
            if s == 'postgresql':
                print('%s: %s'%(s, wad_com.postgresql(cmd, pgdata)))
    
            # wadprocessor is a special case
            elif s == 'wadprocessor':
                print('%s: %s'%(s, wad_com.wadcontrol('quit' if cmd=='stop' else cmd, inifile)))
                
            else:
                if cmd == 'status':
                    print('%s: %s'%(s, wad_com.status(s)))
    
                elif cmd == 'stop':
                    print('%s: %s'%(s, wad_com.stop(s)))
                
                elif cmd == 'start':
                    print('%s: %s'%(s, wad_com.start(s, inifile=inifile)))
    
                elif cmd == 'info':
                    print('%s: %s'%(s, wad_com.info(s)))
    
                elif cmd == 'drop':    
                    error, msg = ensure_postgresql(pgdata)
                    if error:
                        print("ERROR! Command {} needs a running PostgreSQL service for WAD. {}".format(cmd, msg))
                        return
                    print('%s: %s'%(s, wad_com.drop(s, inifile=inifile)))
        

def main():
    # do not run as root! the script will ask for permission if it needs root
    if os.name == 'nt':
        pass
    elif os.geteuid() == 0:
        print("Do not run waddoctor as root! The script will ask you for root permission if it needs it! Exit.")    
        exit(False)

    print("waddoctor v.{}\n".format(__version__))
    print("NOTICE: Changing the state of apache2 or systemd controlled services needs root permissions.\n"
          "If root permissions are needed, you will be prompted for your password.\n\n")

    epilog = ""

    parser = argparse.ArgumentParser(description='wadservices.py version %s'%__version__, epilog=epilog)
    all_drop = ['wadadmin', 'waddashboard']
    all_json = ['config', 'meta']
    
    if 'WADROOT' in os.environ:
        inifile = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')
        pgdata = os.path.join(os.environ['WADROOT'], 'pgsql','data')
    else: # development
        print("WADROOT cannot be found in the environment. Add it to your .bashrc and try again from a new shell.")
        sys.exit()

    parser.add_argument('-i','--inifile',
                        type=str,
                        default=inifile,
                        help='path to wadconfig.ini file [%s]'%(inifile),dest='inifile')

    parser.add_argument('-d','--data',
                        type=str,
                        default=pgdata,
                        help='path to postgresql data folder [%s]'%(pgdata),dest='pgdata')

    parser.add_argument('--drop',
                        type=str,
                        default='',
                        help='Remove the databases of the indicated service. When the indicated service is restarted later, a new default database for that service will be generated. Valid for: {} [{}]'.format(','.join(all_drop), ''),dest='drop')

    parser.add_argument('--dbupgrade',
                        type=str,
                        default='',
                        help='Applies all available upgrades of the indicated database. Valid for: dbwadqc [{}]'.format(''), dest='dbupgrade')

    parser.add_argument('--check',
                        type=str,
                        default='',
                        help='Check status of indicated service. Valid for: dbwadqc [{}]'.format(''), dest='check')

    parser.add_argument('--jsonupgrade',
                        type=str,
                        default='',
                        help='Upgrades the json files in the database to the latest template version. Valid for: config, meta [{}].'.format(''), dest='jsonupgrade')

    args = parser.parse_args()
    inifile = args.inifile
    pgdata = args.pgdata
    
    if len(args.drop) > 0:
        # drop database of wadadmin or waddashboard
        if not args.drop in all_drop:
            print('ERROR! Command "{}" is invalid for service "{}". Aborting, system has not changed!'.format("drop", args.drop) )
            return
        applycmd([args.drop], 'drop', inifile, pgdata)

    elif len(args.dbupgrade) > 0:
        # upgrade database of wadqc
        if not args.dbupgrade in ['dbwadqc']:
            print('ERROR! Command "{}" is invalid for database "{}". Aborting, system has not changed!'.format("dbupgrade", args.dbupgrade) )
            return
        applycmd([args.dbupgrade], 'dbupgrade', inifile, pgdata)
 
    elif len(args.check) > 0:
        # check status of service
        if not args.check in ['dbwadqc']:
            print('ERROR! Command "{}" is invalid for "{}". Aborting, system has not changed!'.format("check", args.check) )
            return
        applycmd([args.check], 'check', inifile, pgdata)

    elif len(args.jsonupgrade) > 0:
        # check status of service
        if not args.jsonupgrade in all_json:
            print('ERROR! Command "{}" is invalid for "{}". Aborting, system has not changed!'.format("jsonupgrade", args.jsonupgrade) )
            return
        applycmd([args.jsonupgrade], 'jsonupgrade', inifile, pgdata)
    else:
        parser.print_help()
    
if __name__ == "__main__":
    main()
