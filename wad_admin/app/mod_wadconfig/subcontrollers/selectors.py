from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, send_file
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs.shared import dbio_connect, INIFILE, bytes_as_string, string_as_bytes
    from ..models import WARuleTags
    from app.libs import selectormaintenance
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.shared import dbio_connect, INIFILE, bytes_as_string, string_as_bytes
    from wad_admin.app.mod_wadconfig.models import WARuleTags
    from wad_admin.app.libs import selectormaintenance
dbio = dbio_connect()

from wad_core.selector import Selector
from wad_qc.connection.pacsio import PACSIO
import os
import tempfile
from io import BytesIO
from peewee import IntegrityError

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_confirm import ConfirmForm
from .forms_selectors import ModifyForm

mod_blueprint = Blueprint('wadconfig_selectors', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/selectors/')
@login_required
def default():
    # display and allow editing of selectors table
    subtitle='Deleting a selector sets off a cascade: It also deletes all entries '\
           'that directly reference that selector, and all other entries that reference those entries. '\
           'That includes: processes, results. The number of references is shown in #processes and #results.'
    include_inactive=True
    stuff = dbio.DBSelectors.select().order_by(dbio.DBSelectors.id)
    if not include_inactive: 
        stuff = stuff.where(dbio.DBSelectors.isactive == True)

    table_rows = []
    for data in stuff:
        meta = html_elements.Link(label="meta", href=url_for('editor.default', mid=data.module_config.meta.id))
        config = html_elements.Link(label=data.module_config.name, href=url_for('editor.default', cid=data.module_config.id))
        table_rows.append([data.id, data.name, 'True' if data.isactive==True else 'False', data.description,
                           data.module_config.module.name,
                           config,
                           meta,
                           len(data.rules),
                           len(data.processes), len(data.results),
                           html_elements.Button(label='clone', href=url_for('.duplicate', gid=data.id)),
                           html_elements.Button(label='delete', href=url_for('.delete', gid=data.id), _class='btn btn-danger'),
                           html_elements.Button(label='edit', href=url_for('.modify', gid=data.id)),
                           html_elements.Button(label='dryrun', href=url_for('.dryrun', gid=data.id)),
                           ])

    table = html_elements.Table(headers=['id', 'name', 'isactive', 'description', 'module_name', 
                                      'view/edit: config', 'view/edit: meta', '#rules', '#processes', '#results'],
                             rows=table_rows,
                             _class='tablesorter-wadred', _id='sortTable')
    newbutton = html_elements.Button(label='New', href=url_for('.modify'))
    page = table+newbutton
    
    return render_template("wadconfig/generic.html", title='Selectors', subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-warning", 'title': "WARNING", 'content':subtitle})

@mod_blueprint.route('/selectors/download')
@login_required
def download():
    # download all data matching with given selector
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    if _gid is None:
        return url_for('.default')

    sel = dbio.DBSelectors.get_by_id(_gid)
    datatype = sel.module_config.data_type.name
    
    # create downloadfolder
    downloadfolder = os.path.join(dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val, 'Download')
    if not os.path.exists(downloadfolder):
        os.mkdir(downloadfolder)
    outfolder = tempfile.mkdtemp(dir=downloadfolder,prefix='sel')
    
    resnum = 0
    results = {}
    # find all matches in results
    for res in sel.results:
        if not res.data_source.id in results.keys():
            results[res.data_source.id] = set() # use sets to avoid double copies of twice processed sets
        results[res.data_source.id].add(res.data_id) 

    # add all matches in processes
    for res in sel.processes:
        if not res.data_source.id in results.keys():
            results[res.data_source.id] = set() # use sets to avoid double copies of twice processed sets
        results[res.data_source.id].add(res.data_id) 
        
    # make sure temp dir exists
    temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)
    
    # make a temporary file; either stream it of write it to disk
    with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
        error, msg, resnum = selectormaintenance.sources_download_datasets(dbio, tmpf, results, datatype, logger)

        if error:
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg=msg,
                                   inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})
            
        if resnum == 0:
            # no data at all
            subtitle='NO datasets selected for {}'.format(sel.name)
            return render_template("wadconfig/generic.html", title='Download data from Sources', subtitle='', msg='', html="",
                                   inpanel={'type': "panel-danger", 'title': "ERROR", 'content':subtitle})
            
        logger.info(msg)
        # Reset file pointer
        tmpf.seek(0)
        return send_file(BytesIO(tmpf.read()), as_attachment=True, attachment_filename='{}_dcm.zip'.format(sel.name))#mimetype=None


@mod_blueprint.route('/selectors/dryrun')
@login_required
def dryrun():
    # display what data would have been selected for the current PACS for a given selector
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    if _gid is None:
        return url_for('.default')

    sel = dbio.DBSelectors.get_by_id(_gid)
    datatype = sel.module_config.data_type.name

    subtitle='If this selector were active, NO datasets would have been selected.'

    include_inactive=True

    wsel = Selector(INIFILE, logfile_only=True)
    for pacs in dbio.DBDataSources.select():
        pacsconfig = pacs.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg=msg,
                                   inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

        table_rows = []
        for studyid in pacsio.getStudyIds():
            wsel.run(pacsconfig['name'], studyid, dryrun=True, datalevel='dcm_study',
                     selectornames=[sel.name], 
                     include_inactive=include_inactive)
            stuff = wsel.getDryrun()
            for data in stuff:
                dataid = data['data_id']
                table_rows.append([pacsconfig['name'], 
                                   html_elements.Link(label=dataid, href=url_for('.showtags',gid=dataid,level=datatype,source=pacsconfig['name']))])

    table = html_elements.Table(headers=['Source', datatype], rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')   

    if len(table_rows) >0:
        subtitle='If this selector were active, these {} unprocessed datasets would have been selected.'.format(len(table_rows))
    
    return render_template("wadconfig/generic.html", title=sel.name, subtitle='', msg='', html=Markup(table),
                           inpanel={'type': "panel-info", 'title': "info", 'content':subtitle})

@mod_blueprint.route('/selectors/realrun')
@login_required
def realrun():
    # Resend all dataset of all DataSources to the WAD Selector for matching with given Selector only
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    if _gid is None:
        return url_for('.default')

    try:
        sel = dbio.DBSelectors.get_by_id(_gid)
    except dbio.DBSelectors.DoesNotExist:
        logger.error("Must supply a valid selector id")
        # go back to overview page
        return redirect(url_for('.default')) # catch wild call

    if sel is None:
        logger.error("Must supply a valid selector id")
        # go back to overview page
        return redirect(url_for('.default')) # catch wild call
        
    datatype = sel.module_config.data_type.name

    if not sel.isactive:
        subtitle='Selector "{}" is not active. Nothing to do.'.format(sel.name)
        return render_template("wadconfig/generic.html", title=sel.name, subtitle='', msg='', html='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':subtitle})

    subtitle='All datasets of all DataSources were resend to the WAD Selector for matching with Selector "{}".'.format(sel.name)

    include_inactive=False # real run, so no inactive selectors!

    wsel = Selector(INIFILE, logfile_only=True)
    for pacs in dbio.DBDataSources.select():
        pacsconfig = pacs.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg=msg,
                                   inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

        for studyid in pacsio.getStudyIds():
            wsel.run(pacsconfig['name'], studyid, dryrun=False, datalevel='dcm_study',
                     selectornames=[sel.name], 
                     include_inactive=include_inactive)

    page = ''
    
    return render_template("wadconfig/generic.html", title=sel.name, subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-success", 'title': "Success", 'content':subtitle})

@mod_blueprint.route('/selectors/showtags')
@login_required
def showtags():
    # display dicom tags of a given dataset
    _source = request.args['source'] if 'source' in request.args else None
    _level = request.args['level'] if 'level' in request.args else None
    _gid = request.args['gid'] if 'gid' in request.args else None
    if not _level in ['dcm_study', 'dcm_series', 'dcm_instance']:
        return redirect(url_for('.default'))
    if _gid is None:
        return redirect(url_for('.default'))
    if _source is None:
        return redirect(url_for('.default'))

    try:
        # 1. make sure a local pacs is running and that it has the required demo data
        pacsconfig = dbio.DBDataSources.get_by_name(_source).as_dict()
        pacsio = PACSIO(pacsconfig)
    except Exception as e:
        msg = ' '.join(['Cannot access PACS %s with provided credentials'%_source,str(e)])
        logger.error(msg)
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg=msg,
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    tags = []
    if _level == 'dcm_study':
        tags = pacsio.getSharedStudyHeaders(_gid)
    elif _level == 'dcm_series':
        tags = pacsio.getSharedStudyHeaders(_gid)
    elif _level == 'dcm_instance':
        tags = pacsio.getInstanceHeaders(_gid)
    
    msg = ''

    table_rows = []
    for key,value in sorted(tags.items()):
        table_rows.append([key,value['Name'],value['Type'],value['Value']])

    table = html_elements.Table(headers=['Tag', 'Name', 'Type', 'Value'], rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')   
    
    subtitle = '%s/%s/%s'%(_source,_level,_gid)
    return render_template("wadconfig/generic.html", title='ShowTags', subtitle=subtitle, msg=msg, html=Markup(table))

@mod_blueprint.route('/selectors/duplicate/')
@login_required
def duplicate():
    """
    clone given id to new selector, changing name
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        nw_sel = dbio.DBSelectors.get_by_id(_gid).clone() # clone already makes new name unique
        nw_sel.isactive = False
        nw_sel.save()

    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/selectors/delete/', methods=['GET', 'POST'])
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    if _gid is None:
        return redirect(url_for('.default'))
    try:
        selectorname = dbio.DBSelectors.get_by_id(_gid).name
    except:
        return redirect(url_for('.default'))
        
    # confirmation dialog
    formtitle = 'Confirm action: delete Selector {}'.format(selectorname)
    msg = [
        'This will delete the Selector and all its Results.',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            title = 'Delete Selector'
            subtitle = 'Successfully deleted Selector "{}". '.format(selectorname)
            msg = ""
            inpanel={'type': "panel-success", 'title': "Success", 'content':subtitle}
            with dbio.db.atomic() as trx:
                try:
                    dbio.DBSelectors.get_by_id(_gid).delete_instance(recursive=True)
                except IntegrityError as e:
                    trx.rollback()
                    subtitle= ['Error deleting Selector {}'.format(selectorname)]
                    msg = str(e)
                    if "violates foreign key constraint" in str(e):
                        subtitle.append(
                            "This Selector is referenced from a table from an external service (e.g. WAD Dashboard). "
                            "Use that service to remove the reference and try again. ")
                        
                    inpanel={'type': "panel-danger", 'title': "ERROR", 'content':subtitle}
            return render_template("wadconfig/generic.html", title='Delete Selector', subtitle='', msg=msg, html="",
                                   inpanel=inpanel)

    return render_template("wadconfig/confirm.html", form=form, action=url_for('.delete', gid=_gid),
                           title=formtitle, msg=msg)

@mod_blueprint.route('/selectors/modify/', methods=['GET', 'POST'])
@login_required
def modify():
    """
    Do not pass cid anymore!
    """
    _gid = None
    _cid = None
    if 'gid' in request.args and not request.args['gid'] is '':
        _gid = int(request.args['gid'])
        try:
            sel = dbio.DBSelectors.get_by_id(_gid)
        except dbio.DBSelectors.DoesNotExist:
            logger.error("Must supply a valid selector id")
            # go back to overview page
            return redirect(url_for('.default')) # catch wild call
    
        if sel is None:
            logger.error("Must supply a valid selector id")
            # go back to overview page
            return redirect(url_for('.default')) # catch wild call
        _cid = sel.module_config.id

    # invalid table request are to be ignored
    formtitle = 'Modify Selector'
    msg = [
        'Fill out the fields and click Submit. ',
        'For a different config, first change only the config and submit before making additional changes. '
        'When adding or deleting rules, submit before making additional changes. '
    ]
    params = None
    rules = None
    limits = {'constraint_equals': None, 'constraint_period': None, 'constraint_minlowhighmax': None, 'constraint_refminlowhighmax': None}
    if not _gid is None:
        params = selectormaintenance.paramsblock(dbio, _cid, logger=logger)
        rules = selectormaintenance.rulesblock(dbio, _gid, logger=logger)
        limits = selectormaintenance.limitsblocks(dbio, _cid, logger=logger)

    form = ModifyForm(None if request.method=="GET" else request.form, actions=params, rules=rules, 
                      equals=limits['constraint_equals'], periods=limits['constraint_period'], 
                      minlowhighmaxs=limits['constraint_minlowhighmax'], 
                      refminlowhighmaxs=limits['constraint_refminlowhighmax'], addnum=0)
    if _gid is None and (form.gid is None or form.gid.data == '' or form.gid.data is None): #define here, to avoid wrong label on redisplaying a form with errors
        formtitle = 'New Selector'
        msg = 'Fill out the fields and click Submit. Afterwards modify the new Selector for SelectorRules etc.'

    # make a list of (id, name), where name is either default + configname or selector+configname, but use on the biggest id if multiple
    default_configs = []
    selector_configs = {}
    for m in dbio.DBModuleConfigs.select():
        if len(m.selectors)>0:
            cname = 'selector: %s'%(m.selectors[0].name)
            if cname in selector_configs:
                selector_configs[cname] = max(selector_configs[cname], m.id)
            else:
                selector_configs[cname] = m.id
        else:
            # exclude configs coupled to results, but not to a current selector (meaning replaced)
            if not m.origin == 'result': 
                default_configs.append((m.id , '%s: %s'%(m.origin, m.name)))
            
    if not _gid is None:
        cname = None
        for key,val in selector_configs.items():
            if val == _cid:
                cname = key
                break
        if not cname is None:
            del(selector_configs[cname])
            selector_configs['<current config>'] = _cid
    
    default_configs = sorted(default_configs, key=lambda x: x[1])
    for key in sorted(selector_configs.keys()):
        default_configs.append((selector_configs[key], key))
    form.module_config.choices = default_configs

    # set tags_choices to updated set
    btnEditConfig = ""
    btnEditMeta = ""
    if not _gid is None:
        sel = dbio.DBSelectors.get_by_id(_gid)
        formtitle += " \"{}\"".format(sel.name)
        # add some buttons to directly edit params and constraints
        btnEditConfig = html_elements.Button(label="Edit Full Config", href=url_for('editor.default', cid=sel.module_config.id), _class='btn btn-danger')
        btnEditMeta = html_elements.Button(label="Edit Full Meta", href=url_for('editor.default', mid=sel.module_config.meta.id), _class='btn btn-danger')
        for dtag in sel.rules:
            if not dtag.dicomtag in [m.val for m in WARuleTags.select()]:
                WARuleTags.create(name=dtag.dicomtag,val=dtag.dicomtag)

    tag_choices = [(m.val, m.name) for m in WARuleTags.select().order_by(WARuleTags.name)]
    for rules_entry in form.rules:
        rules_entry.tag.choices = tag_choices
    
    
    if not _gid is None:
        sel = dbio.DBSelectors.get_by_id(_gid)
        form.name.data = sel.name
        form.currentname.data = sel.name
        form.description.data = sel.description
        form.gid.data = _gid
        form.cid.data = _cid
        form.module_config.data = _cid
        form.isactive.data = sel.isactive

    existingnames = [v.name for v in dbio.DBSelectors.select()]

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.currentname.data is None or form.currentname.data == 'None': # yes, new form
            if form.name.data in existingnames:
                flash('A selector with this name already exist.', 'error')
                valid = False
        else: # not a new form
            if existingnames.count(form.name.data)> (0 if not form.currentname.data == form.name.data else 1) :
                flash('A selector with this name already exist.', 'error')
                valid = False

        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            # check if new or update
            igid = 0
            if 'gid' in field_dict:
                try:
                    igid = int(field_dict['gid'])
                except:
                    pass
            # the elements of the config
            cfg_id = int(field_dict['module_config'])
            if igid>0: # update, not create
                # if we changed the config using the selector, ignore cfg and meta changes
                if not cfg_id == dbio.DBSelectors.get_by_id(igid).module_config.id:
                    # clone the selected config (auto clones meta)
                    nw_config = dbio.DBModuleConfigs.get_by_id(cfg_id).clone()
                    nw_config.origin = 'result'
                    nw_config.save()
                    cfg_id = nw_config.id
                else:
                    cfg_id = selectormaintenance.update_config_if_changed(dbio, cfg_id, field_dict, logger=logger)
                    selectormaintenance.update_meta_if_changed(dbio, cfg_id, field_dict, logger=logger)
                    
            """
            a new selector made with DBSelectors.create() already makes a clone of the config with origin set to 'result'
            """

            blob = None
                
            if igid>0: # update, not create
                sel = dbio.DBSelectors.get_by_id(igid)
                sel.name = field_dict['name']
                sel.description = field_dict['description']
                sel.module_config = cfg_id
                sel.isactive = True if 'isactive' in field_dict else False
                sel.save()

                nwtagval = WARuleTags.get(WARuleTags.id == 1).val # yeah, this will crash if there is no tag left
                if selectormaintenance.handle_rules(dbio, igid, field_dict, nwtagval, logger=logger):
                    url = url_for('.modify')
                    if igid>0:
                        url += '?gid=%d'%igid
                        
                    return redirect(url)
            else:
                dbio.DBSelectors.create(**field_dict)
                
            return redirect(url_for('.default'))
    
    return render_template("wadconfig/selectors_modify.html", form=form, action='.', 
                           title=formtitle, msg=msg, 
                           btnFullConfig=Markup(btnEditConfig), 
                           btnFullMeta=Markup(btnEditMeta))
