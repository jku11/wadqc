from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, send_file
from peewee import BlobField
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs.shared import dbio_connect, INIFILE, bytes_as_string, string_as_bytes, getIpAddress
    from app.libs import dbmaintenance
    from app.libs import dbupgrade as libdbupgrade
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.shared import dbio_connect, INIFILE, bytes_as_string, string_as_bytes, getIpAddress
    from wad_admin.app.libs import dbmaintenance
    from wad_admin.app.libs import dbupgrade as libdbupgrade
dbio = dbio_connect()
    
from wad_qc.connection.pacsio import PACSIO
from wad_core.selector import Selector

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)
import os
from datetime import datetime
import tempfile
from io import BytesIO

import json

# Import modify forms
from .forms_confirm import ConfirmForm
from .forms_qrdelete import QRDForm
from .forms_consistency import ConsistencyForm

mod_blueprint = Blueprint('wadconfig_dbtool', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/dbtool')
@login_required
def default():
    """
    Just present a menu with choices; should all be require authorization
    """
    menus = [
    ]
    menuA =[
        { 'label':'WAD-QC database inspector', 'href':url_for('.inspector') }, 
    ]

    menuC = [
        { 'label':'Check for all datasets in Sources but missing in Processes/Results', 'href':url_for('.consistency',mode='misses') }, 
        { 'label':'Check for dcm_studies in Sources but missing in Processes/Results', 'href':url_for('.consistency',mode='misses', data_type='dcm_study') }, 
    ]
    menuB =[
        { 'label':'upgrade WAD-QC database', 'href':url_for('.dbupgrade') }, 
        { 'label':'truncate results WAD-QC database', 'href':url_for('.truncate') }, 
        { 'label':'empty whole WAD-QC database', 'href':url_for('.cleanstart') }, 
        { 'label':'manage Selectors', 'href':url_for('.manageselector') }, 
    ]

    menuD = [
        { 'label':'download or permanently delete studies from Sources', 'href':url_for('.qrd') }, 
    ]
    
    menus.append({'title':'Monitoring', 'items':menuA})
    menus.append({'title':'Residuals', 'items':menuC})
    menus.append({'title':'Database Maintenance', 'items':menuB})
    menus.append({'title':'Sources Maintenance', 'items':menuD})
    return render_template("wadconfig/home.html", menus=menus, title='WAD-QC DBTool')

def get_source_datatable(sources, dtype, filters, fields, start_date=None, end_date=None):
    """
    helper function to return an embedable table of selected data ids.
    Returns html table, {sourcename:{data_type:[data_ids]}}, number of results
    """
    keys = [ f[1] for f in fields ]
    header_row = [ f[0] for f in fields ]
    table_rows = []

    results = {}
    numresults = 0
    for source in sources:
        pacsconfig = source.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

        results[source.id] = { dtype : []}
        if dtype == 'dcm_series':
            for dataid in pacsio.getSeriesIds(None): # loop over all possible series
                do_include, hdrs = dbmaintenance.source_select_data(pacsio, (pacsconfig['name'], dtype, dataid), filters, return_headers=True, logger=logger)
                if 'series_date' in hdrs:
                    try:
                        series_date = datetime.strptime(hdrs['series_date'], '%Y%m%d').date()
                    except:
                        print(dataid)
                        raise
                    if do_include:
                        if not start_date is None:
                            do_include = series_date >= start_date
                    if do_include:
                        if not end_date is None:
                            do_include = series_date <= end_date
                    
                if do_include:
                    results[source.id][dtype].append(dataid)
                    numresults += 1
                    row = []
                    for k in keys:
                        if k == 'source_name':
                            row.append( pacsconfig['name'] )
                        elif k == 'dtype':
                            row.append( dtype )
                        elif k == 'uid':
                            row.append( html_elements.Link(dataid, url_for('.showtags',gid=dataid,level=dtype, source=pacsconfig['name'])) )
                        else:
                            row.append( hdrs[k] )
                    table_rows.append(row)
    
    table = html_elements.Table(headers=header_row, rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTableNoFilter')
                    

    return table, results, numresults

def _delete_datasets(data):
    """
    data[source_id] = {dtype: [data_id]}
    Permanently delete list of files
    """
    error, msg = dbmaintenance.sources_delete_datasets(dbio, data, logger)
    if error:
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    else:
        return render_template("wadconfig/generic.html", title='Delete from Sources', subtitle='', msg='', html="",
                                   inpanel={'type': "panel-success", 'title': "Success", 'content':msg})

def _download_datasets(data):
    """
    data[source_name] = {dtype: [data_id]}
    Download list of files
    """
    # make sure temp dir exists
    temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)
    
    # make a temporary file; either stream it of write it to disk
    with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
        error, msg, resnum = dbmaintenance.sources_download_datasets(dbio, tmpf, data, logger)

        if error:
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg=msg,
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})
            
        if resnum == 0:
            # no data at all
            subtitle='NO datasets selected for download'
            return render_template("wadconfig/generic.html", title='Download data from Sources', subtitle='', msg='', html="",
                                   inpanel={'type': "panel-success", 'title': "Success", 'content':msg})
            
        logger.info(msg)
        # Reset file pointer
        tmpf.seek(0)

        return send_file(BytesIO(tmpf.read()), as_attachment=True, attachment_filename='{}_dcm.zip'.format('selected_'))#mimetype=None
    


@mod_blueprint.route('/dbtool/db_upgrade_latest')
@login_required
def db_upgrade_latest():
    """
    The real upgrade routine. Ask for some extra params.
    """
    running = request.args.get('running', None)
    latest  = request.args.get('latest', None)
    if running is None or latest is None:
        return url_for('.dbupgrade')

    title = "Upgrade WAD-QC Database"
    subtitle = ""
    page = ""
    msg = ""

    error, msgs = libdbupgrade.upgrade(dbio, logger)
    
    msg = '\n'.join(msgs)
    if error == False:
        inpanel={'type': "panel-success", 'title': "Success", 
                 'content':"Success! Now restart all WAD services for the changes to take effect."}
    else:
        inpanel={'type': "panel-danger", 'title': "ERROR", 
                 'content':msg}

    return render_template("wadconfig/generic.html", title=title, subtitle='', msg=msg, html=Markup(page), inpanel=inpanel)
   

@mod_blueprint.route('/dbtool/dbupgrade')
@login_required
def dbupgrade():
    """
    upgrade database to latest version. 
    ? show a list of defined upgrades, buttons for force redo (make sure this doesn't break)
    show currect version from db, version in dbio, if not same offer upgrade buttons
    """
    title = "Upgrade WAD-QC Database"
    subtitle = ""
    page = ""
    msg = ""

    # versions
    hdr = html_elements.Heading(label="Versions", type="h2")
    col_ok  = {'bgcolor':'yellowgreen'}
    col_bad = {'bgcolor':'red'}

    running_version = libdbupgrade.get_running_version(dbio, logger)
    latest_version  = libdbupgrade.get_latest_version(dbio, logger)
    running_label = '.'.join([str(v) for v in running_version])
    latest_label  = '.'.join([str(v) for v in latest_version])
    
    col_running = col_ok if libdbupgrade.version_equal(running_version, latest_version) else col_bad
    col_latest = col_bad if libdbupgrade.version_smaller(latest_version, running_version) else col_ok

    table_rows = []
    row = ['WAD-QC DataBase']
    row.append({'value': running_label, 'style': col_running})
    row.append({'value': latest_label, 'style': col_latest})
    
    if libdbupgrade.version_smaller(running_version, latest_version):
        btn_upgrade = html_elements.Button(label='Upgrade', href=url_for('.db_upgrade_latest', running=running_label, latest=latest_label), _class='btn btn-primary')
        row.append(btn_upgrade)
        inpanel={'type': "panel-warning", 'title': "WARNING",
                 'content':"The running WAD-QC database needs to be upgraded. Press the upgrade button."}
    elif libdbupgrade.version_equal(running_version, latest_version):
        inpanel={'type': "panel-info", 'title': "info",
                 'content':"The running WAD-QC database is at the latest version. No upgrade needed."}
        
    else:
        inpanel={'type': "panel-warning", 'title': "WARNING",
                 'content':"The running WAD-QC database is incompatible with the installed version of the WAD-QC server. Install the latest version of WAD-QC server!"}
        
        
    table_rows.append(row)
    table = html_elements.Table(headers=['Item',   'Running', 'Latest'], rows=table_rows)
    page = hdr + table

    return render_template("wadconfig/generic.html", title=title, subtitle="", msg=msg, html=Markup(page),
                           inpanel=inpanel)
    
@mod_blueprint.route('/dbtool/qrd', methods=['GET', 'POST'])
@login_required
def qrd():
    # invalid table request are to be ignored
    formtitle = 'Select data from Sources.'
    info = 'For delete you must check the confirm box! Delete will not remove existing Processes or Results.'
    form = QRDForm(None if request.method=="GET" else request.form)
            
    possible_filters = [ # table header, dicom header field, form element
        ('modality', 'modality', 'modality'), ('description', 'series_desc', 'seriesdesc'), ('protocol','prot_name', 'protocol'),
        ('station', 'stat_name', 'station'), ('date', 'series_date'), ('aet', 'aet', 'aet')]

    filters = {}
    for pf in possible_filters:
        if len(pf)>2:
            elem = getattr(form,pf[2],None)
            if elem is None or elem.data is None or elem.data == '' or len(elem.data) == 0:
                continue
            filters[pf[1]] = elem.data
        
    source = getattr(form, 'sourcename', None)
    if not source is None:
        source = source.data
    sources = dbio.DBDataSources.select()
    if not (source is None or source == '' or len(source) == 0):
        sources = [ s for s in sources if source.lower() in s.name.lower() ]

    elem = getattr(form, 'seriesdate_min', None)
    if elem is None or elem.data is None or elem.data == '':
        start_date = None
    else:
        start_date = elem.data

    elem = getattr(form, 'seriesdate_max', None)
    if elem is None or elem.data is None or elem.data == '':
        end_date = None
    else:
        end_date = elem.data

    dtype = 'dcm_series'
    fields = [ ('Source', 'source_name'), # special
              ('data_type', 'dtype'), # special
              ('uid', 'uid'),# special
              ('modality', 'modality'),
              ('series desc', 'series_desc'),
              ('protocol', 'prot_name'),
              ('station', 'stat_name'),
              ('aet', 'aet'),
              ('series date', 'series_date'),
              ] # table header, field key
    if len(filters.keys())>0 or not start_date is None or not end_date is None or not source is None:
        table, results, numresults = get_source_datatable(sources, dtype, filters, fields, start_date, end_date)
        msg = '{} datasets in selection.'.format(numresults)
        page = str(table)
    else:
        table, results, numresults = ('', [], 0)
        msg = 'will not make a selection without filters! '
        page = str(table)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.todo.data == 2 and form.confirm.data is False:
            flash('Must tick confirm to delete!', 'error')
            valid = False

        form.confirm.data = False # whatever action is peformed, make sure confirm is not ticked when reloading!
        if valid and form.todo.data == 1: # download
            if numresults == 0:
                flash('No data in selection, nothing to download!', 'error')
            else:
                return _download_datasets(results)
            
        if valid and form.todo.data == 2: # delete
            if numresults == 0:
                flash('No data in selection, nothing to delete!', 'error')
            else:
                return _delete_datasets(results)

    return render_template("wadconfig/qrd.html", form=form, action=url_for('.qrd'),
                           title=formtitle, info=info, msg=msg, html=Markup(page))
    

@mod_blueprint.route('/dbtool/inspector')
@login_required
def inspector():
    # display and allow editing of modules table
    subtitle=''
    
    tablenames = { model._meta.table_name : model for model in dbio.DBTables } # peewee3: db_table -> table_name
    _gid = request.args['gid'] if 'gid' in request.args else list(sorted(tablenames.keys()))[0]
    
    model = tablenames[_gid]
    stuff = model.select()
    fields = [f for f in model._meta.sorted_field_names]
    
    date_hdr = 'created_at'+20*'&nbsp;'
    header_row = fields
    table_rows = []
    for data in stuff:
        table_rows.append([getvalue(data, f) for f in fields ])
    
    table = html_elements.Table(headers=fields, rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
 
    # make a dropdown for dbtable
    idname = [ [tn, tn] for tn in sorted(tablenames.keys()) ]
    picker = html_elements.Picker(name="dbtable", idname=idname, sid=_gid,
                                  fun=Markup('location.href = "?gid="+jQuery(this).val();'))

    page = picker+table

    return render_template("wadconfig/generic.html", title='DataBase Inspector', subtitle=subtitle, msg='', html=Markup(page))

@mod_blueprint.route('/dbtool/residuals')
@login_required
def residuals():
    return redirect(url_for('.consistency', mode='misses'))

def _getConsistencyTable(mode, filters):
    # display and allow editing of modules table
        
    # aet is only not empty for instances, so do not show
    header_row=[
            'Source', 'data_type', 'uid',
            'patient', 'modality', 'study desc', 'series desc', 
            'protocol', 'station', 'aet', 'study date', 'series date']

    table_rows = []
    used_dtypes = []
    for pacs in dbio.DBDataSources.select():
        pacsconfig = pacs.as_dict()
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(pacsconfig)
        except Exception as e:
            msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacsconfig['name'],str(e)])
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                                   inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

        # _mode == 'misses':
        results = dbmaintenance.source_find_unpicked(dbio, pacsio, pacsconfig['name'], datalevel='all', logger=logger)
            
        # populate results table
        for srcname, dtype, dataid in results:
            do_include, hdrs = dbmaintenance.source_select_data(pacsio, (srcname, dtype, dataid), filters, return_headers=True, logger=logger)

            # restrict output
            if do_include == False: 
                continue

            if not dtype in used_dtypes:
                used_dtypes.append(dtype)
            
            btnDel    = html_elements.Button(label='Delete From PACS', href=url_for('.delete_pacs', src=srcname, datatype=dtype, dataid=dataid), _class='btn btn-danger')

            # _mode == 'misses':
            btnResend = html_elements.Button(label='Resend', href=url_for('.resend_selector', src=srcname, datatype=dtype, dataid=dataid), _class='btn btn-primary')
            table_rows.append([pacsconfig['name'], dtype, 
                               html_elements.Link(dataid, url_for('.showtags',gid=dataid,level=dtype,source=srcname)),
                               hdrs['pat_name'], hdrs['modality'], hdrs['study_desc'], hdrs['series_desc'],
                               hdrs['prot_name'], hdrs['stat_name'], hdrs['aet'], hdrs['study_date'], hdrs['series_date'],
                               btnResend, btnDel])
            
    table = html_elements.Table(headers=header_row, rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
    return table, len(table_rows)

    

@mod_blueprint.route('/dbtool/consistency', methods=['GET', 'POST'])
@login_required
def consistency():
    # display and allow resending/deleting of datasets
    _mode = request.args.get('mode', None)
    if not _mode in ['misses']:
        return redirect(url_for('.default'))

    possible_filters = ['data_type', 'modality', 'pat_name', 'study_desc', 'study_date', 'series_desc', 'prot_name', 'stat_name', 'series_date', 'aet']
    filters = {}
    for filt in possible_filters:
        if filt in request.args:
            filters[filt] = request.args.get(filt, None)
        
    table,nrows = _getConsistencyTable(_mode, filters)
    page = str(table)
 
    # _mode == 'misses':
    # find items in datasources without a match in processes/results
    title = 'Residuals: Not in Processes/Results'
    subtitle='All datasets in all datasources have been picked up.'

    if nrows>0:
        subtitle = [
            'These {} datasets have not been picked up. You could try resending them to the WAD Selector, or delete them from the datasource.'.format(nrows),
            'Use the buttons next to a dataset to resend/delete one dataset. '
            'Use the filterboxes in the table and the buttons below the table to resend/delete a selection of datasets.',
            'Clicking delete will ask for confirmation.'
        ]
   
    form = ConsistencyForm(None if request.method=="GET" else request.form)

    return render_template("wadconfig/consistency.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':subtitle},
                           form=form, action_send=url_for('.send_unpicked'), action_delete=url_for('.delete_unmatched'),
                           label_send="Send shown datasets to WADSelector", label_delete="Delete shown datasets from Sources")

@mod_blueprint.route('/dbtool/showtags')
@login_required
def showtags():
    # display and allow editing of modules table
    _source = request.args['source'] if 'source' in request.args else None
    _level = request.args['level'] if 'level' in request.args else None
    _gid = request.args['gid'] if 'gid' in request.args else None
    if not _level in ['dcm_study', 'dcm_series', 'dcm_instance']:
        return redirect(url_for('.default'))
    if _gid is None:
        return redirect(url_for('.default'))
    if _source is None:
        return redirect(url_for('.default'))

    try:
        # 1. make sure a local pacs is running and that it has the required demo data
        pacsconfig = dbio.DBDataSources.get_by_name(_source).as_dict()
        pacsio = PACSIO(pacsconfig)
    except Exception as e:
        msg = ' '.join(['Cannot access PACS %s with provided credentials'%_source,str(e)])
        logger.error(msg)
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    tags = []
    if _level == 'dcm_study':
        tags = pacsio.getSharedStudyHeaders(_gid)
    elif _level == 'dcm_series':
        tags = pacsio.getSharedStudyHeaders(_gid)
    elif _level == 'dcm_instance':
        tags = pacsio.getInstanceHeaders(_gid)
    
    msg = ''
    table_rows = []
    for key,value in sorted(tags.items()):
        table_rows.append([key, value['Name'], value['Type'], value['Value']])

    table = html_elements.Table(headers=['Tag', 'Name', 'Type', 'Value'], rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')

    subtitle = '%s/%s/%s'%(_source, _level, _gid)

    page = table
    if pacsconfig['typename'] == 'orthanc': # we know how to construct that url
        url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}
        # determine the outside address of this server
        ip = pacsconfig['host']
        if ip == 'localhost':
            ip = getIpAddress(1) # dummy param
        pacs_url = '%s://%s:%s/app/explorer.html'%(pacsconfig['protocol'],
                                                   ip, 
                                                   pacsconfig['port'])
        url = '%s#%s?uuid=%s'%(pacs_url, url_part[_level], _gid)
        btnPACS = html_elements.Button(label='open in PACS', href=url)
        page = btnPACS+page

    #subtitle += str(pacsconfig)
    return render_template("wadconfig/generic.html", title='ShowTags', subtitle=subtitle, msg=msg, html=Markup(page))

@mod_blueprint.route('/dbtool/consistency/resend_selector')
@login_required
def resend_selector():
    _datatype = request.args.get('datatype', None)
    _srcname = request.args.get('src', None)
    _dataid = request.args.get('dataid', None)
    if not _datatype in ['dcm_series', 'dcm_study', 'dcm_instance']:
        return redirect(url_for('.default'))
    
    deleted = False
    # 1. make sure a local pacs is running and that it has the required demo data
    try:
        pacs = dbio.DBDataSources.get_by_name(_srcname)
        pacsio = PACSIO(pacs.as_dict())
    except Exception as e:
        msg = ' '.join(['Cannot access PACS %s with provided credentials'%pacs.name, str(e)])
        logger.error(msg)
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    wsel = Selector(INIFILE, logfile_only=True)
    wsel.run(_srcname, _dataid, dryrun=False, datalevel=_datatype, include_inactive=False)

    page = 'Did send 1 item of data_type "%s" to WAD Selector'%(_datatype)

    return render_template("wadconfig/generic.html", title='Resend to WAD Selector', subtitle='', msg='', html='',
                           inpanel={'type': "panel-success", 'title': "Success", 'content':page})


@mod_blueprint.route('/dbtool/consistency/delete_pacs', methods=['GET', 'POST'])
@login_required
def delete_pacs():
    _datatype = request.args.get('datatype', None)
    _srcname = request.args.get('src', None)
    _dataid = request.args.get('dataid', None)
    if not _datatype in ['dcm_series', 'dcm_study', 'dcm_instance']:
        return redirect(url_for('.default'))
    
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete one {} from {}'.format(_datatype, _srcname)
    msg = [
        'This will permanently delete the selected data set from the QC PACS.',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            src_id = dbio.DBDataSources.get_by_name(_srcname).id
            return _delete_datasets({src_id: {_datatype: [_dataid]}})

    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.delete_pacs', src=_srcname, datatype=_datatype, dataid=_dataid),
                           title=formtitle, msg=msg)

def getvalue(model, field):
    names = ['data_type', 'data_source', 'process_status', 'module', 'selector', 'logic', 'source_type']
    m = getattr(model, field)
    if field in ['pswd', 'token']:
        return('*****')
    if isinstance(model, dbio.DBVariables) and field == 'val' and m.startswith("::PSWD::"):
        return('*****')
        
    if isinstance(m, bytes):
        return ('[blob]')
    if field in names:
        return getattr(m, 'name')
    if hasattr(m, 'id'): #foreign key
        return getattr(m, 'id')
    if field == 'val' and isinstance(m, memoryview):
        return bytes_as_string(m)
    return m
 
@mod_blueprint.route('/dbtool/cleanstart', methods=['GET', 'POST'])
@login_required
def cleanstart():
    """
    Remove whole WAD-QC database, and create a new one.
    """
    # invalid table request are to be ignored
    formtitle = 'Confirm action: empty WAD-QC database.'
    msg = [
        'Empty WAD-QC database will recreate an empty WAD-QC database: all results, '
        'configuration, modules and selectors will be deleted. (DICOM data in PACS will be untouched).',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            error, emsg = dbmaintenance.waddb_cleanstart(dbio, INIFILE, logger)
        
            if error:
                if "violates foreign key constraint" in emsg:
                    subtitle = "Some database elements are referenced from a table from an external service (e.g. WAD Dashboard). "\
                        "Use that service to remove the references and try again. "  
                
                return render_template("wadconfig/generic.html", subtitle=subtitle, msg='', title='WAD-QC DBTool',
                                       inpanel={'type': "panel-danger", 'title': "ERROR", 'content':emsg})
            else:
                return render_template("wadconfig/generic.html", subtitle='', msg='', title='WAD-QC DBTool',
                               inpanel={'type': "panel-success", 'title': "Success", 'content':"Successfully created empty WAD-QC database"})
            
    return render_template("wadconfig/confirm.html", form=form, action=url_for('.cleanstart'),
                           title=formtitle, msg=msg)


@mod_blueprint.route('/dbtool/truncate', methods=['GET', 'POST'])
@login_required
def truncate():
    """
    Truncate most tables in WAD-QC database (deleting all results).
    """
    # invalid table request are to be ignored
    formtitle = 'Confirm action: truncate results tables.'
    msg = [
        'Truncate results tables will empty all results and processes, but leaves '
        'modules and selectors intact. (DICOM data will be untouched in PACS).',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            error, emsg = dbmaintenance.waddb_truncate(dbio, logger)

            if error:
                return render_template("wadconfig/generic.html", subtitle='', msg='', title='WAD-QC DBTool',
                                       inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})
            else:
                return render_template("wadconfig/generic.html", subtitle='', msg='', title='WAD-QC DBTool',
                                       inpanel={'type': "panel-success", 'title': "Success", 'content':"Successfully truncated WAD-QC database"})
            
    return render_template("wadconfig/confirm.html", form=form, action=url_for('.truncate'),
                           title=formtitle, msg=msg)

@mod_blueprint.route('/manageselector/')
@login_required
def manageselector():
    """
    Show number of Processes/Results per selector, and offer to delete them all.
    """
    # display and allow editing of modules table
    subtitle  = [
        '"Delete All" will remove all Processes and Results from the WAD-QC database. '
        'The datasets will not be deleted from the DataSources. Use the Consistency tool to Resend or Delete the data.',
        '"Dryrun" will recheck all DataSources for datasets that could be matched by the Selector.',
        '"Resend All" will resend all datasets of all DataSources to the WAD Selector for this Selector only. '
        'Previous Results/Processes will NOT be removed, so double entries could occur.',
        '"Download" will download all already selected datasets of this Selector.'
    ]
    include_inactive=True
    stuff = dbio.DBSelectors.select().order_by(dbio.DBSelectors.id)
    if not include_inactive: 
        stuff = stuff.where(dbio.DBSelectors.isactive == True)

    header_row = [
            'id', 'name', 'isactive', 'description', 'module_name', 
            'config_name', '#rules', '#processes', '#results']    

    table_rows = []

    for data in stuff:
        table_rows.append([data.id, data.name, 'True' if data.isactive==True else 'False', data.description,
                           data.module_config.module.name,
                           data.module_config.name,
                           len(data.rules),
                           len(data.processes), len(data.results),
                           html_elements.Button(label='Dryrun', href=url_for('wadconfig_selectors.dryrun', gid=data.id), _class='btn btn-primary'),
                           html_elements.Button(label='Resend All', href=url_for('wadconfig_selectors.realrun', gid=data.id), _class='btn btn-warning'),
                           html_elements.Button(label='Delete All', href=url_for('.delete_from_processor', gid=data.id), _class='btn btn-danger'),
                           html_elements.Button(label='Download', href=url_for('wadconfig_selectors.download', gid=data.id), _class='btn btn-primary'),
                           ])
    table = html_elements.Table(headers=header_row, rows=table_rows,
        _class='tablesorter-wadred', _id='sortTable')
    return render_template("wadconfig/generic.html", title='Manage Selectors', subtitle='', msg='', html=Markup(table),
                                   inpanel={'type': "panel-info", 'title': "info", 'content':subtitle})

@mod_blueprint.route('/delete_from_processor/', methods=['GET', 'POST'])
@login_required
def delete_from_processor():
    """
    Delete all Processes/Results of a Selector
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    if _gid is None:
        return url_for('.default')

    try:
        sel = dbio.DBSelectors.get_by_id(_gid)
    except dbio.DBSelectors.DoesNotExist:
        logger.error("Must supply a valid selector id")
        # go back to overview page
        return redirect(url_for('.default')) # catch wild call

    if sel is None:
        logger.error("Must supply a valid selector id")
        # go back to overview page
        return redirect(url_for('.default')) # catch wild call
        
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete {} Results from Selector {}'.format(len(sel.results), sel.name)
    msg = [
        'This will permanently delete the selected Results.',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            processes = 0
            results   = 0
            sel = dbio.DBSelectors.get_by_id(_gid)
            for item in sel.processes:
                item.delete_instance(recursive=True)
                processes += 1
            for item in sel.results:
                item.delete_instance(recursive=True)
                results += 1
        
            # display and allow editing of modules table
            subtitle = ''
            msg  = [
                'Deleted {} Processes'.format(processes),
                'Deleted {} Results'.format(results)
            ]
            page = ''
            
            return render_template("wadconfig/generic.html", title='Delete From Selector', subtitle='', msg='', html=Markup(page),
                                   inpanel={'type': "panel-success", 'title': "Success", 'content':msg})

    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.delete_from_processor', gid=_gid),
                           title=formtitle, msg=msg)

@mod_blueprint.route('/dbtool/consistency/send_unpicked', methods=['GET', 'POST'])
@login_required
def send_unpicked():
    """
    resend all data as shown in filtered consistency table 
    """
    
    wsel = Selector(INIFILE, logfile_only=True)

    num = 0
    datasets = json.loads(request.form.get('resendform-posttable'))

    for srcname, datatype, dataid in datasets:
        num += 1
        wsel.run(srcname.strip(), dataid.strip(), dryrun=False, datalevel=datatype.strip(), include_inactive=False)

    msg = 'Did send {} datasets to WAD Selector'.format(num)
    return render_template("wadconfig/generic.html", title='Send Unpicked', subtitle='', msg='',
                           inpanel={'type': "panel-success", 'title': "Success", 'content':msg})

@mod_blueprint.route('/dbtool/consistency/delete_unmatched', methods=['GET', 'POST'])
@login_required
def delete_unmatched():
    """
    delete all data as shown in filtered consistency table 
    """
    skipvalidate = False
    if 'deleteform-posttable'in request.form:
        datasets = json.loads(request.form.get('deleteform-posttable'))
        skipvalidate = True # skip validate trigger on arrival
    else:
        extra = request.form.get('extradata')
        if extra is None or len(extra) == 0:
            return redirect(url_for('.default'))
        else:
            datasets = json.loads(extra)
            
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete {} data sets from PACS'.format(len(datasets))
    msg = [
        'This will permanently delete the selected data sets from the QC PACS.',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)
    form.extradata.data = json.dumps(datasets)

    # Verify the sign in form
    valid = True
    if not skipvalidate and form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            # make a list per source, per datatype
            smsg = []
            stuff = {}
            for srcname, datatype, dataid in datasets:
                srcname = srcname.strip()
                if not srcname in stuff.keys():
                    stuff[srcname] = {}
                datatype = datatype.strip()
                if not datatype in stuff[srcname].keys():
                    stuff[srcname][datatype] = []
                dataid = dataid.strip()
                stuff[srcname][datatype].append(dataid)

            for srcname,val in stuff.items():
                src_id = dbio.DBDataSources.get_by_name(srcname).id
                num = 0
                for key,va in val.items():
                    num += len(va)
                    
                error, emsg = dbmaintenance.sources_delete_datasets(dbio, {src_id: val}, logger)
                if error:
                    return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                                           inpanel={'type': "panel-danger", 'title': "ERROR", 'content':emsg})
            
                smsg.append('Deleted {} datasets from source "{}"'.format(num, srcname))
                
            if smsg == []:
                smsg.append('Deleted {} datasets'.format(0))

                    
            return render_template("wadconfig/generic.html", title='Delete from Sources', subtitle='', msg='', html="",
                                    inpanel={'type': "panel-success", 'title': "Success", 'content':smsg})


    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.delete_unmatched'),
                           title=formtitle, msg=msg)



