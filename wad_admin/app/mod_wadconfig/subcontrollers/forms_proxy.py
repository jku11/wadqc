# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import HiddenField, TextField

# Import Form validators
from wtforms.validators import Required, NoneOf

class ProxyForm(FlaskForm):
    proxyserver = TextField('proxy-server')
