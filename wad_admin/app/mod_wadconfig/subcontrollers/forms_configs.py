# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import StringField, FileField, HiddenField, SelectField # BooleanField

# Import Form validators
from wtforms.validators import Required, NoneOf

class ModifyForm(FlaskForm):
    name = StringField('name', [Required(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    description = StringField('description', [])
    module = SelectField("module ", coerce=int )
    datatype = SelectField("datatype ", coerce=int )
    configfile = FileField('Config file', [ ])  
    metafile = FileField('Meta file', [ ])  
    currentname = HiddenField('gid', [])
    gid = HiddenField('gid', [])
    
    