# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import PasswordField, HiddenField, BooleanField

# Import Form validators
from wtforms.validators import Required, NoneOf

class CredentialsForm(FlaskForm):
    token = PasswordField('token')#, [Required(message='token cannot be empty!'), NoneOf(['None'],message='token cannot be None!')])
    delete = BooleanField('delete token')
    gid = HiddenField('gid', [])
