# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, session, redirect, url_for
from functools import wraps

# Import password / encryption helper tools
from werkzeug import check_password_hash, generate_password_hash

# Import module forms and models
try:
    from app.mod_auth.forms import LoginForm
    from app.mod_wadconfig.models import WAUsers, role_names
except ImportError:
    from wad_admin.app.mod_auth.forms import LoginForm
    from wad_admin.app.mod_wadconfig.models import WAUsers, role_names

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_auth = Blueprint('auth', __name__, url_prefix='/auth')


# flask provides a "session" object, which allows us to store information across
# requests (stored by default in a secure cookie).  this function allows us to
# mark a user as being logged-in by setting some values in the session data:
def auth_user(user):
    session['logged_in'] = True
    session['user_id'] = user.id
    session['username'] = user.username
    session['refresh'] = user.refresh
    flash('You are logged in as %s' % (user.username), 'message')

# view decorator which indicates that the requesting user must be authenticated
# before they can access the view.  it checks the session to see if they're
# logged in, and if not redirects them to the login view.
# the session will remain logged in until the browser closes
def login_required(f):
    @wraps(f)
    def inner(*args, **kwargs):
        if not session.get('logged_in'):
            return redirect(url_for('auth.signin'))
        return f(*args, **kwargs)
    return inner

# Set the route and accepted methods
@mod_auth.route('/signin/', methods=['GET', 'POST'])
def signin():
    flash('please sign in', 'message')

    # If sign in form is submitted
    form = LoginForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    if form.validate_on_submit():
        try:
            user = WAUsers.get(WAUsers.username==form.username.data)

            if user and check_password_hash(user.password, form.password.data):
                if not role_names.get(user.role, None) == 'admin':
                    flash('Access only allowed for admin', 'error')
                else:
                    auth_user(user)
                    flash('Welcome %s' % user.username, 'message')
                    return redirect(url_for('auth_users.change_default_passwords'))
                    #return redirect(url_for('wadconfig.home'))

            flash('Wrong username or password', 'error')

        except WAUsers.DoesNotExist:
            flash('User does not exist', 'error')
        
    return render_template("auth/signin.html", form=form)

@mod_auth.route('/logout/')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out', 'message')
    return redirect(url_for('wadconfig.home'))
