# Import Form
from flask_wtf import FlaskForm

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import IntegerField, StringField, PasswordField, HiddenField, SelectField, BooleanField, validators
try:
    from app.mod_wadconfig.models import role_names
    from app import AUTO_REFRESH
except ImportError:
    from wad_admin.app.mod_wadconfig.models import role_names
    from wad_admin.app import AUTO_REFRESH

role_choices = [ (k, v) for k,v in sorted(role_names.items()) ]

class ModifyForm(FlaskForm):
    username = StringField('Username', [validators.Length(min=4, max=25,
                                                          message="Username must be between %(min)d and %(max)d characters long.")])
    email    = StringField('Email Address', [validators.Length(min=6, max=35, 
                                                               message="Email Address must be between %(min)d and %(max)d characters long.")])
    role     = SelectField("Role", choices = role_choices, coerce=int )
    isenabled = BooleanField('Enabled')
    password = PasswordField('New Password', [
        #validators.DataRequired('Password is required'),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    refresh = IntegerField('refresh (s)', [validators.NumberRange(min=1, 
                           message="Refresh must be at least %(min)s")],
                           default=AUTO_REFRESH)
    
    gid = HiddenField('gid', [])
    
    confirm = PasswordField('Repeat Password')
