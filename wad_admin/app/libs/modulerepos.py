"""
Routines for downloading and upgrading modules with factory modules in repositories.

This should be a collection of functional stuff for interfacing with WAD-QC through the web interface.

CHECK:
 o delete of config on upgrade for results, selectors, etc.
 o upgrade/overwrite
 
Changelog:
  20180315: Initial version
"""

from __future__ import print_function
import requests
import json
import os
import shutil
import zipfile
import tempfile

try:
    import configparser
except ImportError:
    import ConfigParser as configparser
    
from .exchange import make_factory_zip, import_configs, validate_import_manifest, add_folder_to_zip

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)


# URL of module repository on github
GITHUBREPO = 'https://api.github.com/users/MedPhysQC/repos'

# URL of wadqc repository on bitbucket
WADQCREPO = 'https://api.bitbucket.org/2.0/repositories/MedPhysNL/wadqc'

def set_auth_credits(dbio, url, token, delete, logger=None):
    """
    store the provided credentials for given url
    if delete is True, the entry will be deleted from the database
    """
    if logger is None:
        logger = DummyLogger()

    if delete:
        try:
            elem = dbio.DBVariables.get_by_name(url)
            elem.delete_instance()
        except Exception as e:
            logger.info("Could not delete key {}".format(url))

        logger.debug("Deleted credentials for {}".format(url))
        return
    try:
        elem = dbio.DBVariables.get_by_name(url)
        elem.val = "::PSWD::{}".format(token)
        elem.save()
    except:
        dbio.DBVariables.create(name = url, val = "::PSWD::{}".format(token))
        
    logger.debug("Stored credentials for {}".format(url))

def get_auth_credits(dbio, url=GITHUBREPO, logger=None):
    if logger is None:
        logger = DummyLogger()

    try:
        token = dbio.DBVariables.get_by_name(url).val.split("::PSWD::")[1]
    except:
        token = None
    
    return token

def set_proxyserver(dbio, proxyserver, logger=None):
    """
    store the provided proxy server
    """
    if logger is None:
        logger = DummyLogger()

    try:
        elem = dbio.DBVariables.get_by_name("proxyserver")
        elem.val = proxyserver
        elem.save()
    except:
        dbio.DBVariables.create(name = "proxyserver", val = proxyserver)

    logger.debug("Stored proxyserver: {}".format(proxyserver))

def get_proxyserver(dbio, logger=None):
    if logger is None:
        logger = DummyLogger()

    try:
        proxyserver = dbio.DBVariables.get_by_name("proxyserver").val
    except:
        proxyserver = None

    return proxyserver

def request_with_auth(url, creds, proxyserver, logger=None, stream=False):
    # helper to make a request with authentication if provided
    r = None

    proxyDict = {}
    if proxyserver:
        proxyDict = {
            "http"  : "http://{}".format(proxyserver),
            "https" : "http://{}".format(proxyserver)
        }

    if creds is None:
        r = requests.get(url, headers={"User-Agent":"MedPhysQC"}, stream=stream, proxies=proxyDict)
    elif isinstance(creds, tuple):
        r = requests.get(url, headers={"User-Agent":"MedPhysQC"}, auth=creds, stream=stream, proxies=proxyDict)
    elif isinstance(creds, str):
        r = requests.get(url, headers={"User-Agent":"MedPhysQC", "Authorization": "token {}".format(creds)}, stream=stream, proxies=proxyDict)
    else:
        raise ValueError("Cannot use provided credentials!")

    return r

def is_release_candidate(version):
    """
    check if string resolves to a release candidate
    """
    return split_release_version(version)[2]

def is_newer_release(old, new, format="module"):
    """
    Check if a new release is newer. If old release unknown, return False
    """
    try:
        old_version = split_release_version(old, format)
    except Exception as e:
        return False

    try:
        new_version = split_release_version(new, format)
    except Exception as e:
        return False

    # check major version
    if new_version[0]<old_version[0]:
        return False
    elif new_version[0]>old_version[0]:
        return True

    # major version same, check minor
    if new_version[1]<old_version[1]:
        return False
    elif new_version[1]>old_version[1]:
        return True

    if format == "wadqc":
        # major, minor version same, check micro
        if new_version[2]<old_version[2]:
            return False
        elif new_version[2]>old_version[2]:
            return True
        return False # they are the same
    
    # major and minor version same, check release candidate status
    if not old_version[2] and new_version[2]:
        return False
    elif old_version[2] and not new_version[2]:
        return True
    
    # major and minor version same, check release candidate
    if new_version[3]<old_version[3]:
        return False
    elif new_version[3]>old_version[3]:
        return True
    
    # they are the same
    return False

def split_release_version(rel, format="module"):
    """
    module: Release "v1.0-rc.2" -> (1, 0, True, 2)
    module: Release "v1.0" -> (1, 0, False, 0)
    wadqc: "1.0.0" -> (1, 0, 0)
    """

    if format == "module":
        major = 0
        minor = 0
        rc = False
        rcnum = 0

        try:
            # drop v
            rel = rel[1:]
        
            # get release candidate
            if '-rc.' in  rel:
                rc = True
                rel, rcnum = rel.split('-rc.')
            
            # get major, minor
            major, minor = rel.split('.')
        except Exception as e:
            raise ValueError("{}. Invalid module release version number: {}".format(e, rel))
    
        return (int(major), int(minor), rc, int(rcnum))

    elif format == "wadqc":
        major = 0
        minor = 0
        micro = 0
        try:
            # get major, minor, micro
            major, minor, micro = rel.split('.')
        except Exception as e:
            raise ValueError("{}. Invalid wadqc release version number: {}".format(e, rel))
    
        return (int(major), int(minor), int(micro))

    raise ValueError("Invalid release version number: {}".format(rel))
        

def check_repo(dbio, url=GITHUBREPO, logger=None):
    """
    Query repo for modules, and return dictionary {module_name:(tag_name, published_at, zipball_url)}
    """
    if logger is None:
        logger = DummyLogger()
        
    #First we obtain all the repos that contain releases of analysis modules
    available_modules = {}
    msg = ''
    failed = []

    # get auth
    creds = get_auth_credits(dbio, url, logger)

    # get proxy
    proxyserver = get_proxyserver(dbio, logger)

    json_data = {}
    try:
        r = request_with_auth(url, creds, proxyserver, logger)

        json_data = json.loads(r.text)
    
        #For each repo we obtain release data
        for elem in json_data:
            tmprepo = (elem['name'], elem['releases_url'].replace('{/id}',''))
            #
            tmpr = request_with_auth(tmprepo[1], creds, proxyserver)

            release_data = json.loads(tmpr.text)

            if len(release_data) >0:
                available_modules[elem['name']] = []
            for release in release_data:
                try:
                    available_modules[elem['name']].append({'release_version': release['tag_name'],
                                                            'release_date': release['published_at'],
                                                            'release_url': release['zipball_url'],
                                                            'release_note': release['body'],
                                                            'repo_url': release['url'].split('/releases')[0],
                                                            'repo_dox': "{}/wiki".format(elem['html_url'])})
                except Exception as e:
                    failed.append(elem['name'])
                    logger.error("check_repo failed for {}: {}".format(elem['name'], e))

    except Exception as e:
        logger.error(e)
        if 'message' in json_data:
            msg = json_data['message']
        else:
            msg = "ERROR! Could not query {} for modules. Please try again later or check the proxy settings.".format(url)
        logger.error(msg)

    if len(failed)>0:
        msg += "check_repo failed for: " +", ".join(failed)

    return available_modules, msg


def _url_to_module(dbio, url, creds, repo_info, tempdir, logger=None):
    """
    helper to make a importable module in premade tempdir. checks for consistencies, returns filename. does not delete tempdir.
      url = full url to release e.g. https://api.github.com/repos/MedPhysQC/RF_Pehamed/zipball/v1.2
      creds = authentication credentials or None
      repo_info = {'repo_url': repo_url, 'repo_version': repo_version} of specific module e.g https://api.github.com/repos/MedPhysQC/RF_Pehamed and v1.2
      tempdir = previously created temporay folder; is not removed in this function

    return error, msg, outfile
    """

    if logger is None:
        logger = DummyLogger()
        
    outfile = None
    msg = ""
    error = False

    # get proxy
    proxyserver = get_proxyserver(dbio, logger)

    fname   = os.path.join(tempdir, "mod.zip")
    moddir  = os.path.join(tempdir, "mod")
    os.mkdir(moddir)
    outdir  = os.path.join(tempdir, "out")
    os.mkdir(outdir)
    try:
        # download stream
        r = request_with_auth(url, creds, proxyserver, stream=True)

        # and save to file
        if r.status_code == 200:
            with open(fname, 'wb') as f:
                f.write(r.raw.read())


            # unpack zipfile
            with zipfile.ZipFile(fname, 'r') as z:
                z.extractall(moddir)
            
            # check if 1 manifest.json is present in archive:
            manifests = []
            for subdir, dirs, files in os.walk(moddir, topdown=True):
                for fname in files:
                    if fname == 'manifest.json':
                        manifests.append(os.path.join(subdir, fname))
    
            if not len(manifests) == 1:
                print(manifests)
                error = True
                msg = "ERROR! Number of manifest.json files should be 1. Found {}.".format(len(manifests))
                logger.error(msg)
            
            # now make and import.zip
            if not error:
                error, msg, outfile = make_factory_zip(manifests[0], 'zip_module', 
                                                       repo_info,
                                                       outdir=outdir, logger=logger)
        else:
            error = True
            msg = "ERROR! Could not download from {}. Please try again later. ".format(url)
            logger.error(msg)

    except Exception as e:
        error = True
        msg += "ERROR! {}".format(e)
        logger.error(msg)

    return error, msg, outfile


def install_from_url(dbio, url, repo_info, logger=None):
    """
    Get zipball from url, make factory module and import
      url = full url to release e.g. https://api.github.com/repos/MedPhysQC/RF_Pehamed/zipball/v1.2
      repo_info = {'repo_url': repo_url, 'repo_version': repo_version} of specific module e.g https://api.github.com/repos/MedPhysQC/RF_Pehamed and v1.2
    """
    if logger is None:
        logger = DummyLogger()
        
    msg = ""
    error = False

    # make tmpdir
    temp_root = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_root):
        os.makedirs(temp_root)
    tempdir = tempfile.mkdtemp(dir=temp_root)

    # get credits
    if url.startswith(GITHUBREPO):
        creds = get_auth_credits(dbio, GITHUBREPO, logger)
    else:
        creds = None

    error, msg, outfile = _url_to_module(dbio, url, creds, repo_info, tempdir, logger=logger)
    # import import.zip
    if not error:
        valid, msg = import_configs(dbio, outfile, logger)
        error = not valid

    # clean up
    shutil.rmtree(tempdir, ignore_errors=True)
    if not error:
        return error, "Successfully installed module version {} from {}".format(repo_info['repo_version'], repo_info['repo_url'])
    return error, msg

def replace_from_url(dbio, url, repo_info, mod_id, logger=None):
    """
    Get zipball from url, make factory module. 
    Update old module code with new zipped module.
    Delete old configs, and install new configs.
      url = full url to release e.g. https://api.github.com/repos/MedPhysQC/RF_Pehamed/zipball/v1.2
      repo_info = {'repo_url': repo_url, 'repo_version': repo_version} of specific module e.g https://api.github.com/repos/MedPhysQC/RF_Pehamed and v1.2
    """
    if logger is None:
        logger = DummyLogger()
        
    msg = ""
    error = False

    # the installed module to replace
    current_mod = dbio.DBModules.get_by_id(mod_id)
    current_dbversion = dbio.DBVariables.get_by_name('iqc_db_version').val
    
    # make tmpdir
    temp_root = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_root):
        os.makedirs(temp_root)
    tempdir = tempfile.mkdtemp(dir=temp_root)
    
    # get credits
    if url.startswith(GITHUBREPO):
        creds = get_auth_credits(dbio, GITHUBREPO, logger)
    else:
        creds = None

    # make import.zip
    error, msg, outfile = _url_to_module(dbio, url, creds, repo_info, tempdir, logger=None)

    # extract content of zipball module in module folder
    unpackdir = tempfile.mkdtemp(dir=temp_root)
    
    # unpack zipfile
    with zipfile.ZipFile(outfile, 'r') as z:
        z.extractall(unpackdir)

    # read manifest.ini and check consistency
    with open(os.path.join(unpackdir, 'manifest.json'),'r') as f:
        manifest = json.load(f)
    valid, msg = validate_import_manifest(dbio, unpackdir, check_existing_names=False, logger=logger)

    if valid:
        import_dbversion = manifest['info']['dbversion']
        modules = manifest['modules']
        configs = manifest['configs']

        if len(modules) != 1:
            valid = False
            msg = "ERROR! Expected only 1 module, but import contains {} modules.".format(len(modules))

    # all checks were valid. zip new module and upload it for modification
    if valid:
        try:
            nwmod = list(modules.values())[0]
            nwmod_folder = os.path.join(unpackdir, 'modules', nwmod['folder'])
            zipname = nwmod_folder+'.zip'
            if os.path.exists(zipname):
                os.remove(zipname)
            with zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED) as zf:
                add_folder_to_zip(zf, nwmod_folder, '', mode='zip_module')
        
            current_mod.name        = nwmod['name']
            current_mod.description = nwmod['description']
            current_mod.filename    = nwmod['executable']
            current_mod.origin      = nwmod['origin']
            current_mod.repo_url     = nwmod['repo_url']
            current_mod.repo_version = nwmod['repo_version']
            current_mod.save(uploadfilepath=zipname)
            logger.info('Replaced module {}'.format(nwmod['name']))
                        
        except Exception as e:
            msg = 'Error during import of module "%s". '\
                'DBVersion of imported package is %s (current: %s)'%(nwmod['name'], import_dbversion, current_dbversion)+str(e)
            valid = False
            
    # now remove old ModuleConfigs and upload new ModuleConfigs with MetaConfigs
    #  remove old connected ModuleConfigs, but leave those connected to Processes or Results or Selectors
    del_configs = [ cfg for cfg in current_mod.module_configs if (not len(cfg.selectors) and 
                                                              not len(cfg.processes) and 
                                                              not len(cfg.results)) ]

    for cfg in del_configs:
        cfg.delete_instance(recursive=True)

    if valid:
        try:
            for cfg in configs.values():
                fname = os.path.join(unpackdir, 'configs', cfg['filename'])
                with open(fname,'r') as fcfg: blob = fcfg.read()
                field_dict = {
                    'name': cfg['name'],
                    'description': cfg['description'],
                    'origin': cfg['origin'],
                    'modulename': modules[cfg['module']]['name'],
                    'datatypename': cfg['datatype'],
                    'val': blob
                }
                meta = None
                if 'metafilename' in cfg:
                    fname = os.path.join(unpackdir, 'metas', cfg['metafilename'])
                    with open(fname,'r') as fcfg: meta = fcfg.read()
                if not meta is None:
                    nw_meta = dbio.DBMetaConfigs.create(val=meta)
                else:
                    nw_meta = dbio.DBMetaConfigs.create()
                field_dict['meta'] = nw_meta.id

                dbio.DBModuleConfigs.create(**field_dict)
                logger.info('Imported config {}'.format(cfg['name']))
    
        except Exception as e:
            msg = 'Error during import of module_config "%s". '\
                'DBVersion of imported package is %s (current: %s)'%(cfg['name'], import_dbversion, current_dbversion)+str(e)
            valid = False


    # clean up unpacked zipfile
    shutil.rmtree(unpackdir, ignore_errors=True)
    
    # clean up
    shutil.rmtree(tempdir, ignore_errors=True)

    error = not valid
    if not error:
        return error, "Successfully installed module version {} from {}".format(repo_info['repo_version'], repo_info['repo_url'])
    return error, msg

    
def _check_releases_page(dbio, creds, page, url=WADQCREPO, logger=None):
    """
    Query repo for releases, and return dictionary {module_name:(tag_name, published_at, zipball_url)}
      creds = authentication credits or None
      page = number; number of returned entries is limitted, so page gives the number to start from
    """
    if logger is None:
        logger = DummyLogger()

    # get proxy
    proxyserver = get_proxyserver(dbio, logger)
        
    # obtain all releases in the repo
    msg = ''
    json_data = {}
    
    json_data = {}
    if not page is None:
        url = "{}/downloads?start={}".format(url, page)
    else:
        url = "{}/downloads".format(url)

    try:
        r = request_with_auth(url, creds, proxyserver)

        json_data = json.loads(r.text)

    except Exception as e:
        if 'message' in json_data:
            msg = json_data['message']
        else:
            msg = "ERROR! Could not query {} for releases. Please try again later.".format(url)
        logger.error(msg)

    return json_data, msg


    
def check_releases(dbio, current, url=WADQCREPO, logger=None):
    """
    Query repo for releases, and return dictionary {module_name:(tag_name, published_at, zipball_url)}
      current = current version of release
    """
    if logger is None:
        logger = DummyLogger()
        
    available_releases = []
    report = {}
    msg = ''

    creds = get_auth_credits(dbio, url, logger)

    #First we obtain all releases in the repo
    json_data, msg = _check_releases_page(dbio, creds, page=None, url=url, logger=logger)
    if not msg == '':
        return report, msg

    json_pages = [json_data]
    while json_data.get("isLastPage", True) == False:
        json_data, msg = _check_releases_page(dbio, creds, page=json_data["nextPageStart"], url=url, logger=logger)
        if not msg == '':
            return report, msg
        json_pages.append(json_data)

    failed = []
    
    #For each repo we obtain release data
    for json_data in json_pages:
        for elem in json_data['values']:
            relname = elem['name']
            try:
                version = relname.split('wad_setup_')[1].split('.zip')[0]
                available_releases.append({'name': relname, 
                                           'url': elem['links']['self']['href'],
                                           'version': version})
            except Exception as e:
                logger.error('Invalid release name "{}"'.format(relname))
                failed.append(relname)

    if len(failed)>0:
        msg += "check_release failed for: " +", ".join(failed)

    
    report = {'releases': available_releases}
    if msg == '':
        # check for latest version
        latest = "0.0.0"
        for rel in available_releases:
            if is_newer_release(latest, rel['version'], format="wadqc"):
                latest = rel['version']
                report['latest'] = rel['version']

        report['update_available'] = is_newer_release(current, latest, format="wadqc")
        report['unreleased'] = is_newer_release(latest, current, format="wadqc")
    
    return report, msg


