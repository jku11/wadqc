"""
Routines for upgrading the WAD-QC database.

This should be a collection of functional stuff for interfacing with WAD-QC through the web interface,
and also contain routines needed for running database upgrades from the command line.

Changelog:
  20180316: added  update for factory modules
  20180209: Initial version
"""
from __future__ import print_function
from playhouse.migrate import *

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)


def version_smaller(v1, v2):
    """
    compare a two (major, minor) tuples
    """
    if v1[0]<v2[0]:
        return True
    if v1[0] == v2[0] and v1[1]<v2[1]:
        return True
    return False

def version_equal(v1, v2):
    """
    compare a two (major, minor) tuples
    """
    if v1[0] == v2[0] and v1[1] == v2[1]:
        return True
    return False

def get_running_version(dbio, logger=None):
    """
    Read the version of the database in use. Allow point releases
    """
    if logger is None:
        logger = DummyLogger()

    version = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version').val.split('.')
    major = int(version[0])
    try:
        minor = int(version[1])
    except:
        minor = 0

    return major, minor

def get_latest_version(dbio, logger=None):
    """
    Read the version of the database implemented in dbio. Allow point releases
    """
    if logger is None:
        logger = DummyLogger()

    version = str(dbio.__version__).split('.')
    major = int(version[0])
    try:
        minor = int(version[1])
    except:
        minor = 0

    return major, minor

def _upgrade20171020(dbio, logger):
    """
    Changes:
    DBModuleConfig.origin now takes 3 values: factory, user and result.
     factory: shipped by the wadqc developers (active)
     user: manually imported or user modified (active)
     result: coupled to a selector (active) or a process or result (inactive)
     
    We distinguish 'active' from 'inactive' Configs. Configs that are only coupled to a Process or Result are 'inactive', and are 
    retained for reproducing the coupled Result only. 'active' Configs are 'factory' and 'user' Configs and 'result' Configs
    that are still coupled to a Selector. For creating and modifying Selectors, we are only interested in 'active' Configs.
    
    Implementation:
      1. List all 'user' Configs.
      2. For each 'user' Config, change to 'result' if coupled to a Selector or Process or Result
      
    Caveats:
    Configs that were once coupled to a Selector, but did not Produce any Results or Processes will stay 'user'.
    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20171020"

    # get all configs
    stuff = dbio.DBModuleConfigs.select().order_by(dbio.DBModuleConfigs.id).where(dbio.DBModuleConfigs.origin == 'user')

    # change origin to 'result' if coupled to a Selector or Process or Result
    for cfg in stuff:
        if cfg.origin == 'user':
            if len(cfg.selectors)>0 or len(cfg.processes)>0 or len(cfg.results)> 0:
                cfg.origin = 'result'
                cfg.save()
    
    version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
    version_cur.val = nw_version
    version_cur.save()

    msg = "upgraded to {}".format(version_cur.val)
    error = False
    return error, msg
    

def _upgrade20180209(dbio, logger):
    """
    Add a field "val_ref" to the DBResultFloats table to implement relative constraints
    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20180209"

    if isinstance(dbio.db, PostgresqlDatabase):
        try:
            migrator = PostgresqlMigrator(dbio.db)
            val_ref  = DoubleField(null=True)
    
            migrate(
                migrator.add_column('dbresultfloats', 'val_ref', val_ref),
            )
    
            version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
            version_cur.val = nw_version
            version_cur.save()
        
            msg = "upgraded to {}".format(version_cur.val)
            error = False
        except Exception as e:
            msg = "ERROR! Cannot apply upgrade {}: {}".format(nw_version, str(e))
            error = True
            
    else:
        msg = "ERROR! upgrade {} is only implemented for PostgresSQL!".format(nw_version)
        error = True
        
    return error, msg
"""
    repo_version = CharField(max_length=32 )  # if installed from repo
    repo_url     = CharField(max_length=250 ) # if installed from repo

"""
def _upgrade20180316(dbio, logger):
    """
    Add repository fields to DBModules (repo_version and repo_url)
    repo_version = CharField(max_length=32 )  # if installed from repo
    repo_url     = CharField(max_length=250 ) # if installed from repo

    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20180316"

    if isinstance(dbio.db, PostgresqlDatabase):
        try:
            migrator = PostgresqlMigrator(dbio.db)
            repo_version = CharField(max_length=32, default=""  )
            repo_url     = CharField(max_length=250, default=""  )
            migrate(
                migrator.add_column('dbmodules', 'repo_version', repo_version),
                migrator.add_column('dbmodules', 'repo_url', repo_url),
            )
    
            version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
            version_cur.val = nw_version
            version_cur.save()
        
            msg = "upgraded to {}".format(version_cur.val)
            error = False
        except Exception as e:
            msg = "ERROR! Cannot apply upgrade {}: {}".format(nw_version, str(e))
            error = True
            
    else:
        msg = "ERROR! upgrade {} is only implemented for PostgresSQL!".format(nw_version)
        error = True
        
    return error, msg

def upgrade(dbio, logger):
    """
    Apply all available upgrades in version order, and return all upgrade messages. Stop when an error occurs.
    """
    if logger is None:
        logger = DummyLogger()

    latest_version  = get_latest_version(dbio, logger)
    running_version = get_running_version(dbio, logger)

    error = False
    msgs = []
    while version_smaller(running_version, latest_version):
        if version_smaller(running_version, (20171020,0)):
            error, msg = _upgrade20171020(dbio, logger)
        elif version_smaller(running_version, (20180209,0)):
            error, msg = _upgrade20180209(dbio, logger)
        elif version_smaller(running_version, (20180316,0)):
            error, msg = _upgrade20180316(dbio, logger)
        msgs.append(msg.strip())
        if error:
            return error, msgs
        else:
            logger.info(msg)

        running_version  = get_running_version(dbio, logger)
            
    return error, msgs