#!/usr/bin/env python
from __future__ import print_function

__version__ = '20180131'
"""
Changelog:
  20180131: peewee3 no longer supports set_autocommit and execution_context
"""

import argparse
import os
import time
import threading
import multiprocessing
from multiprocessing.sharedctypes import Value as mpValue
import ctypes
import Pyro4.core
import logging
import logging.handlers
import traceback
from wad_qc.connection import dbio
from wad_core.worker import worker, LOGGERNAMEWORKERS

LOGGERNAME = 'wad_control'

def print_conn():
    # helper to show if unique db connections are used
    #return id(dbio.db.get_conn()) # renamed in peewee3
    return id(dbio.db.connection())
     
def _setup_logging(levelname, wadqcroot, logfile_only):
    # create logger to log to screen and to file
    #logging.basicConfig(format='%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=level)
    loggers = [LOGGERNAME]
    if not LOGGERNAMEWORKERS in loggers:
        loggers.append(LOGGERNAMEWORKERS)

    for logname in loggers:
        loglevel = getattr(logging, levelname)
        logroot = os.path.join(wadqcroot,'Logs')
        if not os.path.exists(logroot):
            os.makedirs(logroot)
        logfile = os.path.join(logroot,'{}.log'.format(logname))
    
        logger = logging.getLogger(logname)
        logger.setLevel(loglevel)
        formatter = logging.Formatter('%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s')

        # restrict handlers to one of each only
        has_filehandler   = False
        has_streamhandler = False
        for hand in logger.handlers:
            if isinstance(hand, logging.FileHandler):
                has_filehandler = True
            elif isinstance(hand, logging.StreamHandler):
                has_streamhandler = True

        if not has_filehandler:
            fh = logging.handlers.RotatingFileHandler(logfile, mode='a', maxBytes=2*1024*1024, backupCount=5) #append mode # max 6*2 MB
            fh.setFormatter(formatter)
            logger.addHandler(fh)

        if not logfile_only and not has_streamhandler:
            ch = logging.StreamHandler()
            ch.setFormatter(formatter)
            logger.addHandler(ch)

   
def resetQueued():
    # on graceful quit of taskmanager, all queued processes should be reset to new
    queued = dbio.DBProcesses.getProcessesByStatus('queued')
    if len(queued)> 0:
        new_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'new')

        with dbio.db.atomic() as txn:
            for proc in queued:
                proc.process_status = new_status
                proc.save()

    msg = '{} queued processes reset to status "new"'.format(len(list(queued)))
    logging.getLogger(LOGGERNAME).info(msg)
        

def forceResetQueue():
    # on graceful quit of taskmanager, all queued processes should be reset to new
    reset_statusses = ['queued', 'busy', 'module error', 'analyser failed']
    procs = dbio.DBProcesses.getProcessesByStatus(reset_statusses)
    if len(procs)> 0:
        new_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'new')
        with dbio.db.atomic() as txn:
            for proc in procs:
                proc.process_status = new_status
                proc.save()

    msg = '{} processes reset to status "new"'.format(len(list(procs)))
    logging.getLogger(LOGGERNAME).info(msg)


def checkUncleanDB():
    # only run on init of TaskManager, to check if there are any processes still queued.
    # If so: exit
    queued_processes = list(dbio.DBProcesses.getProcessesByStatus('queued'))
        
    if len(queued_processes) > 0:
        msg = '{} processes are Queued before start of TaskManager. Exit!'.format(len(queued_processes))
        logging.getLogger(LOGGERNAME).critical(msg)
        raise RuntimeError(msg)

def makeRegistrar():
    """
    http://stackoverflow.com/questions/5707589/calling-functions-by-array-index-in-python/5707605#5707605
    An extra decorator to keep track of exposed methods
    """
    registry = {}
    def registrar(func):
        registry[func.__name__] = func
        return func  # normally a decorator returns a wrapped function, but here we return func unmodified, after registering it
    registrar.all = registry
    return registrar


#instance_mode single is required in order to issue commands from different proxies
@Pyro4.behavior(instance_mode="single")
class TaskManager(object):
    reg = makeRegistrar() # for keeping a dictionary of exposed methodes
    logger = logging.getLogger(LOGGERNAME)
    task_queue = multiprocessing.Queue()

    def __init__(self, daemon, inifile, force_reset = False):
        # make sure the connection is open, else another might close it. this will not create extra connections
        dbio.db_connect(inifile) 
        self.daemon = daemon
        self.inifile = inifile
        self.query_interval = int(dbio.DBVariables.get(dbio.DBVariables.name == 'processor_interval').val)
        self.isrunning = False
        self.workers = []
        self.workers_stopflags = {}
        
        if force_reset:
            forceResetQueue()
            self.quit()
            return

        checkUncleanDB()
        
        self._startWorkers(int(dbio.DBVariables.get(dbio.DBVariables.name == 'processor_workers').val))
        self.start() # Yes do start, no need for external start command
    
    def _startWorkers(self, nstart):
        for i in range(nstart):
            nr = self.getWorkers()+1
            self.logger.info("TM: Adding worker {}".format(nr))
            worker_name = "Worker-{}".format(nr)
            self.workers_stopflags[worker_name] = mpValue(ctypes.c_bool, False)
            p = multiprocessing.Process(target=worker, args=(nr, self.workers_stopflags[worker_name], self.task_queue, self.inifile) )
            p.name = worker_name
            p.daemon = True
            self.workers.append(p)
            p.start()

    def _stopWorkers(self, nstop):
        self.logger.info("TM: Removing {} workers".format(nstop))
        stoplist = [self.workers.pop() for i in range(nstop)]
        for p in stoplist: self.workers_stopflags[p.name].value = True
        for p in stoplist: p.join()
        
    @reg
    @Pyro4.expose
    def setWorkers(self, n):
        n = int(n)
        assert n>=0
        
        if n > self.getWorkers():
            self._startWorkers(n - self.getWorkers())
        if n < self.getWorkers():
            self._stopWorkers(self.getWorkers() - n)

    @reg
    @Pyro4.expose
    def getWorkers(self):
        return len(self.workers)

    @reg
    @Pyro4.expose
    def setQueryInterval(self, interval):
        self.query_interval = float(interval)
        self.logger.info("TM: Query interval set to {}".format(self.query_interval))

    @reg
    @Pyro4.expose
    def getQueryInterval(self):
        return self.query_interval

    @reg
    @Pyro4.expose
    def getQsize(self):
        try:
            return self.task_queue.qsize()
        except NotImplementedError:
            return None

    @reg
    @Pyro4.expose
    def getStatus(self):
        return 'running' if self.isrunning else 'paused'

    @reg
    @Pyro4.expose
    def stop(self):
        self.isrunning = False
        self.logger.info("TM: Stopped...")
    
    @reg
    @Pyro4.expose
    def quit(self):
        self.stop()
        self.setWorkers(0)
        resetQueued()
        self.daemon.shutdown()

    @reg
    @Pyro4.expose
    def queryTaskDB(self):
        # need this context, else every external call will result in a unclosed connection
        # with dbio.db.execution_context(with_transaction=True) as ctx:
        with dbio.db.connection_context() as ctx:
            new = dbio.retry(10, list, dbio.DBProcesses.getProcessesByStatus('new'))        

        if len(new) > 0:
            with dbio.db.transaction() as ctx:
                with dbio.db.atomic() as txn:
                    for proc in new:
                        proc.process_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'queued')
                        proc.save()

        process_ids = [proc.id for proc in new]
        self.logger.info('TM: new processes: '+str(process_ids))
        return process_ids
        
    @reg
    @Pyro4.expose
    def start(self):
        # if already running, do not start a new thread! there should be only one task manager!
        if self.isrunning:
            return

        self.isrunning = True
        def _runthread():
            while self.isrunning:
                self.logger.info("-"*80)
                self.logger.info("TM: Querying Tasks DB")
                for task in self.queryTaskDB():
                    self.task_queue.put(task) # else the queue can be garbage collected
                self.logger.info("TM: Pending tasks: "+str(self.getQsize() or 'unknown'))
                time.sleep(self.query_interval)

        thread = threading.Thread(target=_runthread)
        thread.setDaemon(True)
        thread.start()

    @reg
    @Pyro4.expose
    def help(self):
        """
        List available commands
        """
        msg = 'Available commands:\n'
        for k in sorted(self.reg.all.keys()):
            msg += '\t{}\n'.format(k)
        return(msg)

def tryConnection():
    """
    Try to make a connection to a running Pyro4 server.
    """
    logger = logging.getLogger(LOGGERNAME)
    Pyro4.config.SERIALIZER = 'json'
    Pyro4.config.COMMTIMEOUT = 5      # set for all Pyro4 instances to 5 seconds
    uri = None

    file_list = []
    if 'WADROOT' in os.environ:
        file_list.append(os.path.join(os.environ['WADROOT'], 'WAD_QC', 'pyro_uri'))
    file_list.append('pyro_uri')

    for furi in file_list:
        try:
            with open(furi, 'r') as f:
                uri = f.read()
                # Try to communicate
                try:
                    msg = "Testing for {} seconds with file {} if a WAD-QC TaskManager (Pyro4 server) is already running...".format(Pyro4.config.COMMTIMEOUT, furi)
                    logger.info(msg)
                    Pyro4.Proxy(uri).getStatus()
                    return True
                except Pyro4.errors.CommunicationError:
                    # Cannot connect: not running yet
                    continue

        except IOError as e:
            continue

    if uri is None:
        return False


def main():
    parser = argparse.ArgumentParser(description='WAD-QC TaskManager')

    if 'WADROOT' in os.environ:
        inifile = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')
    else:
        inifile = 'wadconfig.ini'
    if not os.path.exists(inifile):
        inifile = None

    force_reset = False
    logfile_only = False
    parser.add_argument('-i', '--inifile',
                        default=inifile,
                        type=str,
                        help='the inifile file of the WAD server if not using the default location',
                        dest='inifile')
    parser.add_argument('--force_reset',
                        default=force_reset,action='store_true',
                        help='Reset all running, queued and failed processes to "new" and exit',dest='force_reset')
    parser.add_argument('--logfile_only',
                        default=logfile_only,action='store_true',
                        help='Suppress output to terminal, only to logfile (use when starting this from a script)',dest='logfile_only')

    args = parser.parse_args()

    try:
        dbio.db_connect(args.inifile)
    except:
        msg = 'ERROR! cannot connect to WAD-QC database with {}'.format(args.inifile)
        print(msg)
        return msg
    
    wadqcroot = dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val
    loglevel = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_logging').val

    _setup_logging(loglevel, wadqcroot, args.logfile_only)
    logger = logging.getLogger(LOGGERNAME)

    if tryConnection():
        msg = 'Already running...'
        logger.critical(msg)
        exit()

    daemon = Pyro4.Daemon()
    uri = daemon.register(TaskManager(daemon, args.inifile, args.force_reset))
    if args.force_reset:
        return
    
    with open('pyro_uri', 'w') as f:
        f.write(str(uri))

    msg = "WAD-QC TaskManager is Ready..."
    logger.info(msg)
    daemon.requestLoop()
    daemon.close()

if __name__ == "__main__":
    main()
