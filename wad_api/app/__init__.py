#imports
from flask import Flask, jsonify, request, Response
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity, jwt_optional
from wad_qc.connection import dbio
from wad_qc.connection.dbio_models import DBDataSources
import os
import datetime
import json
import jsmin
import codecs
import base64
from functools import wraps

# Use wad_admin login
from werkzeug import check_password_hash, generate_password_hash
try:
    from app.mod_wadconfig.models import WAUsers, role_names
except ImportError:
    from wad_admin.app.mod_wadconfig.models import WAUsers, role_names

#connection info to WAD DB
INIFILE = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')

# Define the WSGI application object
flask_app = Flask(__name__)

# Generate a random secret key for signing cookies
import uuid
flask_app.config['SECRET_KEY'] = generate_password_hash(str(uuid.uuid4()))
jwt = JWTManager(flask_app)

flask_app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=5)
flask_app.config['JWT_HEADER_TYPE'] = 'JWT'


@flask_app.before_request
def before_request():
    if dbio.db.is_closed():
        try:
            dbio.db_connect(INIFILE)
        except Exception as e:
            raise RuntimeError("Cannot connect to WADQC")

@flask_app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin','*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response

@flask_app.teardown_request
def teardown_request(exception):
    if not dbio.db.is_closed:
        dbio.db.close()


# helper functions
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

# database helpers
def get_selectors():
    response = []
    selectors = dbio.DBSelectors.select().order_by(dbio.DBSelectors.name)
    for selector in selectors:
        response.append({
            'id':selector.id,
            'name':selector.name,
            'isactive':selector.isactive,
            'description':selector.description
            })
    return response

def get_results_for_selector(id_selector):
    selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    response = []
    for result in selector.results:
        if len(result.datetimes)>0:
            response.append({'id':result.id, 'date':max([datetime.val for datetime in result.datetimes])})
        else:
            response.append({'id':result.id, 'date':result.created_time})
    return response

def get_result_for_selector(id_result):
    result = dbio.DBResults.get(dbio.DBResults.id==id_result)
    meta = json.loads(jsmin.jsmin(bytes_as_string(result.selector.module_config.meta.val)))
    response=[]
    if len(result.datetimes)>0:
        for item in result.datetimes:
            test = get_status_for_test(item.id,'datetime')
            response.append(test)
    if len(result.floats)>0:
        for item in result.floats:
            test = get_status_for_test(item.id,'float')
            response.append(test)
    if len(result.strings)>0:
        for item in result.strings:
            test = get_status_for_test(item.id,'string')
            response.append(test)
    if len(result.objects)>0:
        for item in result.objects:
            t = item.val
            base64EncodedStr = base64.b64encode(t)
            response.append({'name':item.name, 
                             'value':base64EncodedStr, 
                             'limit':'', 'status':0, 
                             'type':'object', 
                             'id':item.id})
    return response

def get_status_for_test(id_test, rtype):
    units = ''
    display_name = ''
    if rtype=='float':
        test = dbio.DBResultFloats.get(dbio.DBResultFloats.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        val = test.val
        if test.name in meta['results']:
            if 'constraint_minlowhighmax' in meta['results'][test.name]:
                limits = meta['results'][test.name]['constraint_minlowhighmax']
                limit = limits
                if not limits[3] is None and test.val>limits[3]:
                    status = 3
                elif not limits[0] is None and test.val<limits[0]:
                    status = 3
                elif not limits[2] is None and test.val>limits[2]:
                    status = 2
                elif not limits[1] is None and test.val<limits[1]:
                    status = 2
                else:
                    status = 1

            elif 'constraint_refminlowhighmax' in meta['results'][test.name]:
                limits = meta['results'][test.name]['constraint_refminlowhighmax']
                if limits[0] is None:
                    status = 0
                    limit = None
                else:
                    limit = [(limits[0]+limits[0]*(float(limits[1])/100)) if not limits[1] is None else None, 
                             (limits[0]+limits[0]*(float(limits[2])/100)) if not limits[2] is None else None, 
                             (limits[0]+limits[0]*(float(limits[3])/100)) if not limits[3] is None else None, 
                             (limits[0]+limits[0]*(float(limits[4])/100)) if not limits[4] is None else None]
                
                    if not limit[3] is None and test.val>limit[3]:
                        status = 3
                    elif not limit[0] is None and test.val<limit[0]:
                        status = 3
                    elif not limit[2] is None and test.val>limit[2]:
                        status = 2
                    elif not limit[1] is None and test.val<limit[1]:
                        status = 2
                    else:
                        status = 1

            else:
                status = 0
                limit = None

            if 'units' in meta['results'][test.name]:
                units = meta['results'][test.name]['units']
            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
        else:
            status = 0
            limit = None
            
    elif rtype=='bool':
        test=dbio.DBResultBools.get(dbio.DBResultBools.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        val = test.val
        if test.name in meta['results']:
            if 'constraint_equals' in meta['results'][test.name]:
                limit = meta['results'][test.name]['constraint_equals']
                if limit==test.val:
                    status = 1
                else:
                    status = 3
            else:
                limit = None
                status = 0

            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']

        else:
            limit = None
            status = 0

    elif rtype=='datetime':
        test = dbio.DBResultDateTimes.get(dbio.DBResultDateTimes.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        val = test.val
        if test.name in meta['results']:
            if 'constraint_period' in meta['results'][test.name]:
                limit = meta['results'][test.name]['constraint_period']
                t = (datetime.datetime.now()-test.val)
                t = t.days+t.seconds/60/60/24
                if t>limit:
                    status = 3
                else:
                    status = 1
            else:
                limit = None
                status = 0
            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
        else:
            limit = None
            status = 0
    elif rtype=='string':
        test = dbio.DBResultStrings.get(dbio.DBResultStrings.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        val=test.val
        if test.name in meta['results']:
            if 'constraint_equals' in meta['results'][test.name]:
                limit = meta['results'][test.name]['constraint_equals']
                if limit==test.val:
                    status = 1
                else:
                    status = 3
            else:
                limit=None
                status = 0
            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
        else:
            limit = None
            status = 0
    elif rtype=='object':
        test=dbio.DBResultObjects.get(dbio.DBResultObjects.id==id_test)
        meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))
        t = test.val
        val = base64.b64encode(t)
        limit = None
        status = 0
        if test.name in meta['results']:
            if 'display_name' in meta['results'][test.name]:
                display_name = meta['results'][test.name]['display_name']
    else:
        print(id_test, rtype)
    return {'name':test.name,'id':test.id,'type':rtype,'limit':limit,
            'status':status,'value':val,'units':units,'display_name':display_name}

def get_status_for_result(result):
    status = {}
    status_list = []
    for item in result:
        if item['type']=='datetime':
            status['datetime'] = item['status']
        else:
            status_list.append(item['status'])
    try:
        status['tests'] = max(status_list)
    except:
        status['tests'] = 0

    return status

def get_datetime_for_result(result):
    dt = None
    for item in result:
        if item['type']=='datetime':
            dt = item['value']
    return dt

def get_selector(id_selector):
    selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    return selector

def get_result(id_result):
    result = dbio.DBResults.get(dbio.DBResults.id==id_result)
    return result

def get_data_for_result(id_result):
    result = get_result(id_result)
    data_id = result.data_id
    data_type = result.module_config.data_type.name
    return (data_type, data_id)

def get_current_user_role(username):
    try:
        user = WAUsers.get(WAUsers.username==username)
        if user:
            return role_names.get(user.role)
    except WAUsers.DoesNotExist:
        return False

# route functions
def _login(username, password):
    """
    Use WAUsers to authenticate.
    First check password, then check if REST access is granted
    """
    try:
        user = WAUsers.get(WAUsers.username==username)

        if user:
            if check_password_hash(user.password, password):
                if user.status == 0:
                    return {'success':False, 'msg':'User not activated!'}
                if role_names.get(user.role, None) in [None, 'admin']:
                    return {'success':False, 'msg':'User has no REST access! {} {}'.format(user.role, role_names.get(user.role, None))}
               
                return {'success':True, 'msg':'Welcome!'}
            else:
                return {'success':False, 'msg':'Wrong password!'}
        else:
            return {'success':False, 'msg':'Wrong username!'}

    except WAUsers.DoesNotExist:
        return {'success':False, 'msg':'Wrong username!'}

# Decorators
def required_roles(*roles):
    def wrapper(f):
        @wraps(f)
        @jwt_optional
        def wrapped(*args,**kwargs):
            valid_user = False
            valid_role = False
            if 'Authorization' in request.headers:
                if request.headers['Authorization'].startswith("JWT"):
                    username = get_jwt_identity()
                    if get_current_user_role(username) not in roles:
                        return jsonify({'success':False, 'msg':"User not authorized!"})
                    else:
                        return f(*args,**kwargs)
                elif request.headers['Authorization'].startswith("Basic"):
                    username = (request.authorization["username"])
                    password = (request.authorization["password"])
                    valid_login = _login(username,password)
                    if valid_login:
                        if get_current_user_role(username) not in roles:
                            return Response('Please authenticate...', 401, {'WWW-Authenticate':'Basic realm="User not authorized!"'})
                        else:
                            return f(*args,**kwargs)
            else:
                return Response('Please authenticate...', 401, {'WWW-Authenticate':'Basic realm="Authentication Required"'})
        return wrapped
    return wrapper

# Routes
# catch all undefined routes
@flask_app.route('/', defaults={'path': ''}, methods=['POST', 'GET', 'DELETE', 'HEAD', 'PUT', 'OPTIONS'])
@flask_app.route('/<path:path>', methods=['POST', 'GET', 'DELETE', 'HEAD', 'PUT', 'OPTIONS'])
def catch_all(path):
    return jsonify({'success':False, 'msg':"'{}' is not a valid endpoint".format(path)})

@flask_app.route('/api/authenticate', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"success":False, "msg":"No logindata given"}),400
    username=request.json.get('username', None)
    password=request.json.get('password', None)
    if not username:
        return jsonify({"success":False, "msg":"No user given"}), 400
    if not password:
        return jsonify({"success":False, "msg":"No password given"}), 400

    loginresponse = _login(username, password)
    if loginresponse['success']:
        access_token = create_access_token(identity=username)
        return jsonify({'token':access_token, 'success':True})
    else:
        return jsonify(loginresponse)
        
@flask_app.route('/api/verifytoken', methods=['POST'])
@jwt_required
def verifytoken():
    user = get_jwt_identity()
    if get_jwt_identity():
        return jsonify({"success":True, "msg":"Welcome back", "rest_full":get_current_user_role(user)=='rest_full'})
    else:
        return jsonify({"success":False, "msg":"Invalid or no token"})

@flask_app.route('/api/selectors', methods=['GET'])
@required_roles('rest_full','rest_major')
def route_get_selectors():
    selectors = get_selectors()
    return jsonify({'success':True, 'selectors':selectors})

@flask_app.route('/api/selectors/<int:id_selector>', methods=['GET'])
@required_roles('rest_full','rest_major')
def route_get_selectors_id(id_selector):
    selector = get_selector(id_selector)
    return jsonify({'success':True, 
                    'selector':{'id':selector.id, 'name':selector.name, 'isactive':selector.isactive}
                    })

@flask_app.route('/api/selectors/<int:id_selector>/results', methods=['GET'])
@required_roles('rest_full','rest_major')
def route_get_selector_id_results(id_selector):
    selector = get_selector(id_selector)
    results = get_results_for_selector(id_selector)
    return jsonify({'success':True, 'results':results, 
                    'selector':{'id':selector.id,'name':selector.name}
                    })

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>', methods=['GET'])
@required_roles('rest_full','rest_major')
def route_get_selector_id_results_id(id_selector, id_result):
    selector = get_selector(id_selector)
    result = get_result_for_selector(id_result)
    status = get_status_for_result(result)
    dt = get_datetime_for_result(result)
    return jsonify({'success':True, 
                    'result':{'id':id_result, 'status':status, 'date':dt},
                    'tests':result,
                    'selector':{'id':selector.id, 'name':selector.name}})

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/dicom',methods=['GET'])
@required_roles('rest_full','rest_major')
def route_get_data_for_result(id_selector, id_result):
    (data_type, data_id) = get_data_for_result(id_result)
    return jsonify({'success':True, 'data_type':data_type, 'data_id':data_id})

@flask_app.route('/api/selectors/<int:id_selector>/results/last', methods=['GET'])
@required_roles('rest_full','rest_major')
def route_get_selector_id_results_last(id_selector):
    selector = get_selector(id_selector)
    results = get_results_for_selector(id_selector)
    if len(results) == 0:
        return jsonify({'success':True,
                        'result':{},
                        'tests':None,
                        'selector':{'id':selector.id, 'name':selector.name}})
        
    last_result = results[0]
    for result in results:
        if result['date']>last_result['date']:
            last_result = result
    id_result = last_result['id']
    date_result = last_result['date']
    result = get_result_for_selector(id_result)
    status = get_status_for_result(result)
    return jsonify({'success':True,
                    'result':{'id':id_result, 'date':date_result, 'status':status},
                    'tests':result,
                    'selector':{'id':selector.id, 'name':selector.name}})

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:rtype>', methods=['GET'])
@required_roles('rest_full','rest_major')
def route_get_selector_id_results_id_tests_id_type(id_selector, id_result, id_test, rtype):
    selector = get_selector(id_selector)
    result = get_result_for_selector(id_result)
    status = get_status_for_result(result)
    dt = get_datetime_for_result(result)
    test = get_status_for_test(id_test, rtype)
    return jsonify({'success':True,
                    'selector':{'id':selector.id, 'name':selector.name},
                    'result':{'id':id_result, 'status':status, 'date':dt},
                    'test':test})
                    
@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:rtype>/history', methods=['GET'])
@required_roles('rest_full','rest_major')
def route_get_selector_id_results_id_tests_id_type_history(id_selector, id_result, id_test, rtype):
    historic_data=[]
    if rtype=='float':
        test = dbio.DBResultFloats.get(dbio.DBResultFloats.id==id_test)
        model = dbio.DBResultFloats
    elif rtype=='string':
        test = dbio.DBResultStrings.get(dbio.DBResultStrings.id==id_test)
        model = dbio.DBResultStrings
    elif rtype=='bool':
        test = dbio.DBResultBools.get(dbio.DBResultBools.id==id_test)
        model = dbio.DBResultBools
    elif rtype=='object':
        test = dbio.DBResultObjects.get(dbio.DBResultObjects.id==id_test)
        model = dbio.DBResultObjects
    elif rtype=='datetime':
        test = dbio.DBResultDateTimes.get(dbio.DBResultDateTimes.id==id_test)
        model = dbio.DBResultDateTimes

    items = model.select().join(dbio.DBResults).join(dbio.DBSelectors).where((dbio.DBSelectors.id == id_selector) & (model.name == test.name) )
    for item in items:
        dts = item.result.datetimes
        if len(dts)>0:
            dt = max([p.val for p in dts])
        else:
            dt = item.result.created_time
        historic_data.append({'date':dt,'val':item.val,'id':item.id})

    sortorder=request.args.get('sort','desc')
    historic_data.sort(key=lambda r: r['date'],reverse=(sortorder=='desc'))
    limit=request.args.get('limit',False)
    if limit:
        historic_data=historic_data[0:int(limit)]
    return jsonify({'success':True,
                    'history':historic_data,
                    'test':{'name':test.name}})                    

@flask_app.route('/api/wadselector', methods=['GET'])
#@auth_required
def wadselector():
    import subprocess
    
    studyid = request.args.get('studyid')
    source = request.args.get('source')
        
    if studyid is None:
        return jsonify({'success':False, 'msg':'Missing studyid'})
        
    if source is None:
        return jsonify({'success':False, 'msg':'Missing source'})

    try:
        src = DBDataSources.get(DBDataSources.name==source)

    except DBDataSources.DoesNotExist:
        return jsonify({'success':False, 'msg':"Unregistered Source '{}'".format(source)})

    selector_wrapper = os.path.join(os.environ['WADROOT'], 'orthanc', 'lua', 'wadselector.py')
    cmd = [selector_wrapper, '--source', source, '--studyid', studyid, '--inifile', INIFILE, '--logfile_only']

    subprocess.Popen(cmd)

    return jsonify({'success':True, 'msg':'Study send to wadselector'})

@flask_app.route('/api/testaccess_full', methods=['GET'])
@required_roles('rest_full')
def testaccess_full():
    """
    test if access is possible
    """
    return jsonify({'success':True, 'msg':'rest_full access'})

@flask_app.route('/api/testaccess_major', methods=['GET'])
@required_roles('rest_full', 'rest_major')
def testaccess_major():
    """
    test if access is possible
    """
    return jsonify({'success':True, 'msg':'rest_major access'})

@flask_app.route('/api/testaccess_minor', methods=['GET'])
@required_roles('rest_full', 'rest_major', 'rest_minor')
def testaccess_minor():
    """
    test if access is possible
    """
    return jsonify({'success':True, 'msg':'rest_minor access'})