#!/usr/bin/env python
from __future__ import print_function
try:
    from libs.exchange import make_factory_zip
except ImportError:
    from wad_admin.app.libs.exchange import make_factory_zip

"""
Generate a factory module: make an import.zip file, containing the module and all defined configs set to origin=factory
Try to reuse stuff in wad_admin/libs/exchange

First run a wad_update.sh, then do point this file to the git folder version
Use the git path, not the dev path!

Changelog:
  20180315: moved functional code to wadadmin for repo
  20180220: fixed missing json files (only file names)

"""
__version__ = '20180315'
__author__ = 'aschilham'

import argparse
import sys
import os.path as path
import os
import zipfile
import json
import jsmin

## main
if __name__=="__main__":
    parser = argparse.ArgumentParser(description=
            'WAD-QC 2.0 Factory Module Import Maker version %s.' 
            'Use it to generate an importable zips (containing module and configs) for uploading to the WAD server.'
            'E.g. if the manifest.json of a module is at path/module/manifest.json, invoke' 
            '  make_factory_module.py -i path/module/manifest.json' 
            ' to create mymodule.py.zip containing all .py and .exe files in dapath'%__version__)

    infilename = None
    mode = 'list'
    parser.add_argument('-i','--input',
                        default=infilename, type=str,
                        help='path to the manifest.json of module', dest='infilename')

    parser.add_argument('-d','--dest',
                        default=None, type=str,
                        help='path to the destination folder', dest='outdir')

    parser.add_argument('-m','--mode',
                        default=mode, type=str,
                        help='run mode (list or zip) [{}]'.format(mode), dest='mode')

    modes = ['list', 'zip']

    args = parser.parse_args()
    if args.mode not in modes:
        print('ERROR! "{}" is not a valid mode!\n\n'.format(args.mode))
        parser.print_help()
        sys.exit()
    mode = "{}_module".format(args.mode)

    if args.infilename is None:
        print('ERROR! "{}" is not a module manifest.json file!\n\n'.format(args.infilename))
        parser.print_help()
        sys.exit()

    make_factory_zip(args.infilename, mode, repo_info={}, outdir=args.outdir, logger=None)