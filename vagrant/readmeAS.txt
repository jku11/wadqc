# url
https://app.vagrantup.com/ubuntu/boxes/bionic64
https://www.vagrantup.com/intro/getting-started/provisioning.html

QuickStart
==========
# install vagrant
sudo apt install vagrant
(sudo apt remove vagrant-libvirt)

# make a new machine:
go to folder ubuntu or centos and type:
vagrant up (this will download the base image if not already present, restart a halted image or start a new one if none exists)


General Commands
================
# get base image and create Vagrantfile to edit later
vagrant init ubuntu/bionic64

# start machine
vagrant up

# enter machine
vagrant ssh

# Shut down the VM
vagrant halt

# Hibernate the VM
vagrant suspend

#Destroy the VM
vagrant destroy

# network (add to Vagrantfile)
  config.vm.network "forwarded_port", guest: 12001, host: 12001
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.network "forwarded_port", guest: 3000, host: 3000
  config.vm.network "forwarded_port", guest: 11112, host: 11112
  config.vm.network "forwarded_port", guest: 8042, host: 8042
  
# scp (remove vagrant-libvirt to prevent problems)
vagrant plugin install vagrant-scp
vagrant scp file_on_host /tmp/
vagrant scp  ~/wad_setup_2.0.11.zip /tmp/

Example
=======
cd ubuntu18
vagrant up
vagrant scp  ~/wad_setup_2.0.11.zip /tmp/
vagrant ssh
 su waduser
 
