#!/usr/bin/env bash

yum update
yum install -y unzip sudo
groupadd waduser
useradd -s /bin/bash --no-log-init -r -g waduser waduser
echo 'waduser:waddemo' | chpasswd
cp -r /etc/skel /home/waduser
chown -R waduser:waduser /home/waduser
usermod -aG wheel waduser

# disable selinux
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
