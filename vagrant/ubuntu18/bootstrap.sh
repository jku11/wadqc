#!/usr/bin/env bash

apt update
apt install --no-install-recommends -y unzip
groupadd waduser
useradd -s /bin/bash --no-log-init -r -g waduser waduser
echo 'waduser:waddemo' | chpasswd
cp -r /etc/skel /home/waduser 
chown -R waduser:waduser /home/waduser
adduser waduser sudo
