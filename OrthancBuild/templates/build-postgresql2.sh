#!/bin/bash

# Orthanc - A Lightweight, RESTful DICOM Store
# Copyright (C) 2012-2015 Sebastien Jodogne, Medical Physics
# Department, University Hospital of Liege, Belgium
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


set -e

# Get the number of available cores to speed up the builds
COUNT_CORES=`grep -c ^processor /proc/cpuinfo`
echo "Will use $COUNT_CORES parallel jobs to build PostgreSQL plugin"

# Clone the repository and switch to the requested branch
cd __DEVROOT__/orthanc/sources/
hg clone https://bitbucket.org/sjodogne/orthanc-databases/
cd orthanc-databases
echo "Switching to branch: $1"
hg up -c "$1"

# Build the plugin
mkdir Build
cd Build
__MACFLAGS__ cmake -DCMAKE_BUILD_TYPE=Release -DALLOW_DOWNLOADS=ON __FIXPGFLAGS__ -DSTATIC_BUILD=ON -DUSE_GTEST_DEBIAN_SOURCE_PACKAGE=OFF -DUSE_SYSTEM_JSONCPP=OFF __DEVROOT__/orthanc/sources/orthanc-databases/PostgreSQL

make -j$COUNT_CORES
cp -L libOrthancPostgreSQLIndex.so __DEVROOT__/orthanc/plugins/
cp -L libOrthancPostgreSQLStorage.so __DEVROOT__/orthanc/plugins/

