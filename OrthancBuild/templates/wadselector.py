#!/usr/bin/env python
import subprocess

__version__ = '20160419'

"""
wadselector should be the process that runs the real selector in the background,
because the lua script in orthanc halts orthanc during execution of the lua script 
(https://orthanc.chu.ulg.ac.be/book/users/lua.html : All of these callbacks are 
guaranteed to be invoked in mutual exclusion), meaning that the selector cannot
access orthanc.
"""

if __name__ == "__main__":
    import os
    import sys
    
    cmd = ['__DEVROOT__/WAD_QC/wad_core/selector.py']
    cmd.extend(sys.argv[1:])

    if os.path.exists(cmd[0]): # first try if we are in development mode
        subprocess.Popen(cmd,cwd=os.path.dirname(os.path.abspath(cmd[0])))
    else:
        cmd[0] = 'wadselector'
        subprocess.Popen(cmd)
