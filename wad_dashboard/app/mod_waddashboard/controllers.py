# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, session, redirect, url_for, Markup

try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs import libgroups, libdisplay
    from app.libs.shared import dbio_connect, INIFILE
    from .models import WDMainGroups, WDSections, WDSubGroups, WDMainItems, WDUserGroups, WDUsers, WDSubItems
except ImportError:
    from wad_dashboard.app.mod_auth.controllers import login_required
    from wad_dashboard.app.libs import html_elements
    from wad_dashboard.app.libs import libgroups, libdisplay
    from wad_dashboard.app.libs.shared import dbio_connect, INIFILE
    from wad_dashboard.app.mod_waddashboard.models import WDMainGroups, WDSections, WDSubGroups, WDMainItems, WDUserGroups, WDUsers, WDSubItems

import datetime
from dateutil.relativedelta import relativedelta

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

dbio = dbio_connect()
    
# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_blueprint = Blueprint('waddashboard', __name__, url_prefix='/waddashboard')

def get_visible_groups():
    """
    returns a list of visible MainGroups for the currently logged in user
    """
    if session.get('logged_in'):
        role = session.get('role')
        user = WDUsers.get_by_id(session.get('user_id'))
    else:
        return []
    if role>0:
        usergroups = [ug.maingroup for ug in WDUserGroups.select().where(WDUserGroups.user == user) ]
    else:  
        usergroups = WDMainGroups.select().order_by(WDMainGroups.name)

    return usergroups

def is_maingroup_visible(maingroup):
    """
    maingroup is a WDMainGroups instance
    """
    return maingroup in get_visible_groups()

def is_subgroup_visible(subgroup):
    """
    subgroup is a WDSubGroups instance
    """
    if session.get('logged_in'):
        role = session.get('role')
        if role == 0:
            return True

    for group in get_visible_groups():
        for sg in WDMainItems.select().where(WDMainItems.maingroup == group):
            if subgroup == sg.subgroup:
                return True

    return False

def is_selector_visible(selector):
    """
    selector is a DBSelectors instance
    """
    if session.get('logged_in'):
        role = session.get('role')
        if role == 0:
            return True

    for group in get_visible_groups():
        for sg in WDMainItems.select().where(WDMainItems.maingroup == group):
            for sel in WDSubItems.select().where(WDSubItems.subgroup == sg.subgroup):
                if selector == sel.selector:
                    return True

    return False

# Set the route and accepted methods
@mod_blueprint.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    """
    Just present a menu with choices; should all be require authorization
    """

    usergroups = get_visible_groups()

    menustoprow = []
    menus = [
    ]
    
    totalmenu=[
        { 'label':'QC dashboard', 'href':url_for('.qc') }, 
    ]

    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role == 0:
        totalmenu.append(
            { 'label':'All active selectors', 'href':url_for('.qc', all_selectors=1) }
        )   

    menustoprow.append({'title':'Overview', 'items':totalmenu})
    for section in WDSections.select().order_by(WDSections.position):
        title = section.name
        vizgroups = section.maingroups.order_by(WDMainGroups.name)

        selection = []
        for gr in vizgroups:
            if gr in usergroups:
                selection.append(gr)
        if len(selection) == 0:
            continue
        vizgroups = selection

        items = [ {'label':i.name , 'href':url_for('.qc', group=i.name),
                   'subitems': [{'label': si.subgroup.name, 'href':url_for('.qc',group=i.name, subgroup=si.subgroup.name)} 
                                for si in WDMainItems.select().join(WDSubGroups).where(WDMainItems.maingroup == i).order_by(WDSubGroups.name) ] } 
                  for i in vizgroups ]

        menus.append({'title':title, 'items':items})
        
    return render_template('waddashboard/home.html', menus=menus, menustoprow=menustoprow, title='WAD-QC Dashboard')

def show_this_qc(latest_date, production, latest_status, filters):
    """
    return True or False for filter selected
    """
    # no filters, must show
    if len(filters) == 0:
        return True

    # determine each status and the worst as well
    do_show = True
    worst = -1
    status = {}
    for key,elem in [('date',latest_date), ('production',production), ('status',latest_status)]:
        try:
            if '<div style="background-color:{}"'.format(libdisplay.COLOR_CRITICAL) in elem:
                status[key] = 2
            elif '<div style="background-color:{}"'.format(libdisplay.COLOR_WARNING) in elem:
                status[key] = 1
            elif '<div style="background-color:{}"'.format(libdisplay.COLOR_OK) in elem:
                status[key] = 0
        except TypeError:
            status[key] = 0 # assume 'good' for any elem without a color (status)
            
        worst = max(worst, status[key])
        if status[key] >= filters.get(key, -1):
            do_show = do_show & True
        else:
            do_show = False

    # if 'any' defined, then report only worst
    if 'any' in filters.keys():
        if worst >= filters['any']:
            return True
        else:
            return False
    
    return do_show 
            
@mod_blueprint.route('/qc')
@login_required
def qc():
    # show a table with selectors:
    #   name, description, latest acq date (red/green), latest qc status (red/green), status production (red/green)

    usergroups = get_visible_groups()

    msg=''
    _group = request.args.get('group', None)
    _subgroup = request.args.get('subgroup', None)
    _separate = 1 if 'separate' in request.args else None
    _all_selectors = (str(request.args.get('all_selectors', None)) == "1")
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role > 0 and _all_selectors: # only for admin
        _all_selectors = False

    filters = {}
    possible_filters = ['date', 'status', 'production', 'any']
    for key in possible_filters:
        if key in request.args:
            try:
                filters[key] = int(request.args[key])
            except:
                pass
    if filters.get('any',-1)> -1: # remove all other filters
        filters = {'any':filters['any']}

    add_refresh = int(request.args.get('refresh', session.get('refresh')))
    
    # status of processes
    num_status = {}
    nprocs = 0
    
    results = []
    subgroups = []
    if not _subgroup is None:
        grps = [ sg.maingroup for sg in WDMainItems.select().join(WDSubGroups).where(WDSubGroups.name == _subgroup) ]
        for grp in grps:
            if grp in usergroups:
                subgroups = [_subgroup]
                break

    elif not _group is None:
        if WDMainGroups.get_by_name(_group) in usergroups:
            subgroups = sorted([ sg.subgroup.name for sg in WDMainItems.select().join(WDMainGroups).where(WDMainGroups.name == _group) ])
    else:
        # subgroup is None, group is None
        subgroups = set()
        for group in usergroups:
            for sg in WDMainItems.select().where(WDMainItems.maingroup == group):
                subgroups.add( sg.subgroup.name )
        subgroups = sorted(list(subgroups))
        
    if _all_selectors:
        res, stu = groupoverview(filters, None, None, True) 
        results.extend(res)
    
        # find number of jobs still in processes table
        procs = dbio.DBProcesses.select().where(dbio.DBProcesses.selector << stu)
        for data in procs:
            if not data.process_status.name in num_status:
                num_status[data.process_status.name] = 0
            num_status[data.process_status.name] += 1
        nprocs += len(procs)
    else:    
        for sub in subgroups:
            res, stu = groupoverview(filters, _group, sub, not (_separate is None)) 
            results.extend(res)
    
            # find number of jobs still in processes table
            procs = dbio.DBProcesses.select().where(dbio.DBProcesses.selector << stu)
            for data in procs:
                if not data.process_status.name in num_status:
                    num_status[data.process_status.name] = 0
                num_status[data.process_status.name] += 1
            nprocs += len(procs)
        
    msg = 'Number of processes still in the queue: {}'.format(nprocs)
    if nprocs>0:
        for key,val in num_status.items():
            msg += "\n\t{}: {}".format(key,val)
    
    table_rows = []
    for res in results:
        table_rows.append(res)

    table = html_elements.Table(headers=['name', 'description', 'latest date', 'latest status', 'production 6m'],
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')

    # add some status pickers
    stat_vals = [
        ('', -1),
        ('ok', 0),
        ('warning', 1),
        ('critical', 2)
    ]
    pickers = Markup()
    for fil in possible_filters:
        # make a dropdown selector
        linkopt = {'refresh': add_refresh}
        if _all_selectors:
            linkopt['all_selectors']=1
        if _separate:
            linkopt['separate']=1
            
        if not _group is None and not _subgroup is None:
            link = '"{}"'.format(url_for('.qc', group=_group, subgroup=_subgroup, **linkopt))
        elif not _group is None:
            link = '"{}"'.format(url_for('.qc', group=_group, **linkopt))
        elif not _subgroup is None:
            link = '"{}"'.format(url_for('.qc', subgroup=_subgroup, **linkopt))
        else:
            link = '"{}"'.format(url_for('.qc', **linkopt))

        if not fil == 'any': # for 'any' filter, reset other options to None
            for fil2 in possible_filters:
                if not fil2 == 'any': # for not-'any' filters, reset 'any' to None
                    if not fil2 == fil and fil2 in filters.keys():
                        link += '+"&{}="+jQuery("#{}").val()'.format(fil2, 'filter{}'.format(fil2))
        
        pname = 'filter{}'.format(fil)
        idname = [ [val, '{} >= {}'.format(fil, stat)] for stat,val in stat_vals ]
        picker = html_elements.Picker(name=pname, idname=idname, sid=filters.get(fil,-1),
                                      fun=Markup('location.href = {}+"&{}="+jQuery(this).val();'.format(link, fil)))
                
        pickers += picker

    # add a checkbox to ungroup all subgroups; only if there are subgroups in this view
    btnSeparateSelectors = ""
    if not _all_selectors and len(subgroups)>0: 
        btnLabel =  'Join Selectors' if _separate == 1 else 'Separate Selectors'
        btnFlags = {'refresh': add_refresh}
        if not _group is None: btnFlags['group'] = _group 
        if not _subgroup is None: btnFlags['subgroup'] = _subgroup
        if _separate is None: btnFlags['separate'] = 1
        btnSeparateSelectors = html_elements.Button(label=btnLabel, href=url_for('.qc', **btnFlags ))

    page = pickers+btnSeparateSelectors+table
    
    if _group is None and _subgroup is None:
        if _all_selectors:
            subtitle = 'All selectors'
        else:
            subtitle = ''
    elif _subgroup is None:
        subtitle = _group
    elif _group is None:
        subtitle = _subgroup
    else:
        subtitle = "{}/{}".format(_group, _subgroup)

    return render_template('waddashboard/generic.html', title='WAD-QC Status', msg='', subtitle=subtitle, 
                           html=Markup(page), add_refresh=add_refresh,
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})

def groupoverview(filters, group=None, subgroup=None, separate=False):
    """
    return a status page for a group or subgroup
    optionally the seperate flag is to return all selectors instead of one group result
    """
    subgroups = [] # sets of selectors
    selectors = [] # loose selectors
    if group is None and subgroup is None:
        # no group and no subgroup given, so describe all selectors
        include_inactive=False
        selectors = dbio.DBSelectors.select().order_by(dbio.DBSelectors.name)
        if not include_inactive: 
            selectors = selectors.where(dbio.DBSelectors.isactive == True).order_by(dbio.DBSelectors.name)
        subgroups = WDSubGroups.select()
        
    else:
        if subgroup is None: 
            # group but no subgroup given, so describe all loose selectors in group
            subgroups = [ it.subgroup for it in WDMainItems.select().join(WDMainGroups).where(WDMainGroups.name == group) ]
        else:
            # subgroup given, describe only selectors in sub group
            subgroups = [ WDSubGroups.get_by_name(subgroup) ]
        
        for dasubgroup in subgroups:
            subitems = dasubgroup.subitems
            for subitem in subitems:
                try:
                    selectors.append(dbio.DBSelectors.get_by_id(subitem.selector))
                except dbio.DBSelectors.DoesNotExist: # happens if a selector was deleted in WAD_Admin
                    subitem.delete_instance(recursive=False)
    
    # give results for subgroups and loose selectors
    results = []

    stuff = [] # list of all used selectors
    if separate == False:
        # first add results for subgroups
        for dasubgroup in subgroups:
            subitems = dasubgroup.subitems
            sel_ids = []
            for subitem in subitems:
                try:
                    sel_ids.append(subitem.selector)
                except dbio.DBSelectors.DoesNotExist: # happens if a selector was deleted in WAD_Admin
                    subitem.delete_instance(recursive=False)
    
            partstuff = dbio.DBSelectors.select().where(dbio.DBSelectors.id << sel_ids).order_by(dbio.DBSelectors.name)
            stuff.extend(partstuff)
    
            # join results of all selectors into one result
            latest_id, latest_date, production, latest_status = libgroups.getLatestCombinedAcqResults(dbio, partstuff, logger=logger)
            if show_this_qc(latest_date, production, latest_status, filters):
                results.append([html_elements.Link(label=dasubgroup.name, href=url_for('results.showgroupresults', subid=dasubgroup.id, rid=0)),
                                dasubgroup.description,
                                html_elements.Div(label=latest_date),
                                latest_status,
                                production,
                                html_elements.Button(label='timeline', href=url_for('.timeline',subid=dasubgroup.id))
                                ])
            
    else:
        # add results for separate selectors
        selectors = sorted(selectors, key=lambda x: x.name)
        stuff.extend(selectors)
        for sel in selectors:
            latest_id, latest_date, production, latest_status = libgroups.getLatestCombinedAcqResults(dbio, [sel], logger=logger)
            if show_this_qc(latest_date, production, latest_status, filters):
                results.append([html_elements.Link(label=sel.name, href=url_for('results.showresults', rid=latest_id)),
                                   sel.description,
                                   html_elements.Div(label=latest_date),
                                   latest_status,
                                   production,
                                   html_elements.Button(label='timeline', href=url_for('.timeline',sid=sel.id))
                                   ])

    return results, stuff

@mod_blueprint.route('/timeline')
@login_required
def timeline():
    # show a table: interval, created_at, status,  # in interval
    msg=''
    _sid = int(request.args['sid']) if 'sid' in request.args else None
    _subid = int(request.args['subid']) if 'subid' in request.args else None

    if _sid is None and _subid is None:
        logger.error("Invalid request. Missing parameters sid and subid.")
        return(redirect(url_for('.home')))
    _days = int(request.args['days']) if 'days' in request.args else int(366./2+.5)
    
    hdrs = ['start -- end', 'submissions', 'latest status']
    if _subid is None:
        selector = dbio.DBSelectors.get_by_id(_sid)
        if not is_selector_visible(selector):
            logger.error("Invalid request. Selector is not visible to current user.")
            return(redirect(url_for('waddashboard.home')))

        subtitle = '%s: %s'%(selector.name, selector.description)
        selectors = [selector]
    else:
        hdrs = ['start -- end', 'complete submissions', 'latest status']
        subgroup = WDSubGroups.get_by_id(_subid)
        if not is_subgroup_visible(subgroup):
            logger.error("Invalid request. Subgroup is not visible to current user.")
            return(redirect(url_for('waddashboard.home')))
        
        subtitle = '%s: %s'%(subgroup.name, subgroup.description)
        selectors = [ sub.selector for sub in subgroup.subitems ]

    table_rows = []
    stuff, period = libgroups.getTimeLine(dbio, selectors, _days, logger=logger)
    if period is None:
        logger.warn("There is nothing period defined for the timeline.")
    if len(stuff) == 0:
        logger.warn("There is nothing to show in the timeline.")
        #return redirect(url_for('.home'))
    for data in stuff:
        table_rows.append([ "{} -- {}".format(data[0].strftime('%Y-%m-%d'), data[1].strftime('%Y-%m-%d')), data[2], data[3]])

    table = html_elements.Table(headers=hdrs, rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
        
    if period is None:
        msg = 'There is nothing to show because there is no constraint_period defined.'
    else:
        subtitle += ' period = %d days'%(period)
        msg = ''
        if len(stuff) == 0:
            if period>_days:
                msg = 'There is nothing to show because the constraint_period is larger than the checked time interval ({}days > {}days).'.format(period, _days)

    # add a picker for reporting interval
    fun = Markup(
        'var qd = {};'\
        'location.search.substr(1).split("&").forEach(function(item) {var s = item.split("="), k = s[0], v = s[1] && decodeURIComponent(s[1]); (k in qd) ? qd[k].push(v) : qd[k] = [v]});' \
        'location.href = %s+"&days="+jQuery(this).val();'%('"?sid="+qd["sid"]' if _subid is None else '"?subid="+qd["subid"]'))
    idname = [ [ds, label] for ds, label in [(int(366./2+.5), 'half a year'), (366, 'one year'), (int(2*366), 'two years')] ]
    picker = html_elements.Picker(name="interval", idname=idname, sid=_days, label="Time interval:",
                                  fun=fun)

    page = picker+table
    
    if len(msg) > 1:
        return render_template('waddashboard/generic.html', title='WAD-QC Timeline', subtitle=subtitle, msg='', html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})
    else:
        return render_template('waddashboard/generic.html', title='WAD-QC Timeline', subtitle=subtitle, msg=msg, html=Markup(page))
        
def getLatestCreated(selector):
    processes = selector.processes
    results = selector.results
    latest = None
    if len(processes)> 0:
        latest = max([p.created_time for p in processes])
        
    if len(results)> 0:
        latest_result = max([p.created_time for p in results])
        if latest is None:
            latest = latest_result
        else:
            latest = max(latest, latest_result)
    
    return latest
