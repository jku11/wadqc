# Import Form 
from flask_wtf import FlaskForm 

# Import Form elements such as TextField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, TextField, SelectField, IntegerField

# Import Form validators
from wtforms.validators import Required, NoneOf

class SelectorEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    sid = HiddenField()
    sel_name = HiddenField()
    sel_description = HiddenField()
    sel_isactive = HiddenField()
    sel_selected = BooleanField('include in group')

class SubGroupEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    sub_id = HiddenField()
    sub_name = HiddenField()
    sub_description = HiddenField()
    sub_selected = BooleanField('include in group')

class GroupForm(FlaskForm):
    gid = HiddenField('gid', [])
    section = SelectField('section', coerce=int )
    name = TextField('group name', [Required(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    description = TextField('group description')
    subgroups = FieldList(FormField(SubGroupEntryForm))

class SectionForm(FlaskForm):
    gid = HiddenField('gid', [])
    name = TextField('name', [Required(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    description = TextField('description')
    position = IntegerField('position', [Required(message='Position cannot be empty!')])

class SubGroupForm(FlaskForm):
    gid = HiddenField('gid', [])
    name = TextField('subgroup name', [Required(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    description = TextField('subgroup description')
    selectors = FieldList(FormField(SelectorEntryForm))
